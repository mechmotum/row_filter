Introduction
============

This repository provides a set of Jupyter notebooks and a Python package
``row_filter`` and that reproduces the analysis in:

   B. Cloud, B. Tarien, A. Liu, T. Shedd, X. Lin, M. Hubbard, P. Crawford, and
   J. Moore, "Adaptive smartphone-based sensor fusion for estimating
   competitive rowing kinematic metrics", 2019.

A preprint of the article can be viewed here: https://engrxiv.org/nykuh

The source for the paper can be found here: https://gitlab.com/mechmotum/row_filter_paper

The software is archived here:

.. image:: https://zenodo.org/badge/DOI/10.5281/zenodo.3378965.svg
   :target: https://doi.org/10.5281/zenodo.3378965

The accompanying data can be found here: https://doi.org/10.6084/m9.figshare.7963643.v2

License
=======

The source code of this repository is licensed under the MIT software license.
We also include one MIT licensed file from:

   Duarte, M., 2015, "Notes on Scientific Computing for Biomechanics and Motor
   Control", https://github.com/demotu/BMC

Any figures in this repository are licensed under the CC-BY 4.0 license.

How to Use
==========

Step 1: Install Anaconda or Miniconda
-------------------------------------

Anaconda is a Python distribution and it provides the conda environment and
package manager. It provides an easy way to install the latest version of
Python and Python packages. It is recommended to install miniconda from
https://conda.io/miniconda.html.

The ``row_filter`` package depends on the following packages:

- Python >=3.5
- NumPy
- SciPy
- Pandas
- pyproj

They can be installed by typing::

   $ conda install -c conda-forge "python>=3.5" numpy scipy pandas pyproj

To run the notebooks you must install these additional dependencies:

- Jupyter
- DynamicistToolKit (only available via the conda-forge channel)
- Matplotlib
- Seaborn
- Plotly
- SymPy
- filterpy (only available via the conda-forge channel)

Similarly, type::

   $ conda install -c conda-forge jupyter dynamicisttoolkit matplotlib seaborn plotly sympy filterpy

Step 2: Clone the repository
----------------------------

::

   $ git clone git@gitlab.com:a9503006/paper_row.git
   $ cd paper_row

Step 2.5 (optional): Setting up a development environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

If you want to work on developing the ``row_filter`` package it is recommended to
setup a Conda environment with all the dependencies and then install the row
filter as a development package using setuptools.

::

   $ conda env create -f env-dev.yml
   $ conda activate row-filter-dev
   (row-filter-dev)$ cd /location/on/my/computer/of/row_filter  # directory with setup.py
   (row-filter-dev)$ python setup.py develop

This allows you to run the notebooks in the notebook directory and see the
results of changes in the package files instantly.

If you edit the ``row_filter`` package, you must recreate the csv files using
the following notebooks:

1. ``stroke_detect-paper.ipynb``: Run this notebook to create a csv file with
   data for the stroke dataframe.
2. ``Normalize_Stroke.ipynb``: Run this notebook to create a csv file with the
   data for normalizing the strokes

Step 3: Get the Data
--------------------

Download ``row_data.zip`` from:

https://doi.org/10.6084/m9.figshare.7963643.v2

and unzip unzip the files into this repository's ``notebooks`` directory.

::

   $ unzip /path/to/row_data.zip -d /path/to/row_filter/notebooks/

Optional precomputed data
~~~~~~~~~~~~~~~~~~~~~~~~~

The stroke data frame is also in the private "Rowing" google drive folder
titled ``all_stroke.csv``. This datafile can be used to run
``create_stroke_dataframe.ipynb`` without running all of the algorithms for
each trial (which can take up to 15 minutes).

The normalization data frame is also in the private "Rowing" google drive
folder titled ``all_stroke.csv``. This datafile can be used to run
``create_norm_dataframe.ipynb``.

Step 4: Create intermediate data
-------------------------

Run the following script on the command line before running the notebooks:

::

    $ python generate_intermediate_data.py

Notebook descriptions
=====================

1. accuracy_precision_of_time_series.ipynb: Defines and formulates the accuracy
   and precision of the time series.
2. calculate_R.ipynb: Calculate the covariance matrix with the row data.
3. compare_cutoff_freqs.ipynb: Uses the averages of all the optimal cutoff
   frequencies to determine the best average cutoff frequency to use for
   real-time implementation. The averages of the frequencies are generated
   using the optimize_cutoff_freq_ext_pos.ipynb and
   optimize_cutoff_freq_ext_vel.ipynb notebooks.
4. create_norm_plots.ipynb: After generating the intermediate data, this
   notebook will create the percent stroke plots found in the paper.
5. create_stroke_dataframe.ipynb: Uses the csv file created in
   stroke_detect-paper to make graphs
6. diffGPS_accuracy.ipynb: ???
7. optimize_cutoff_freq_ext_pos.ipynb: Optimizes the complementary filter
   velocity value.
8. optimize_cutoff_freq_ext_vel.ipynb: Optimizes the complementary filter
   distance value.
9. single_pass_plots.ipynb: Creates all single pass plots

``row_filter`` package module descriptions
==========================================

``clean_data.py``

Routine data manipulation on Phone and Differential GPS data so that data can
be easily analyzed and compared.

``signal_sync_error.py``

All functions pertaining to time synchronization errors between Unix time and
GPS time.

``kalman.py``

Applies a Kalman filter to the phone data.

``complementary.py``

Applies a Complementary filter to the phone data. (Britt to explain different
filters)

Stroke Level Analysis
---------------------

``clean_data_stroke_accz.py``

Data manipulation to detect strokes using acc_z signal and build a data frame.

``clean_data_stroke_waist.py``

Data manipulation to detect strokes using waist signal and build a data frame.

``detect_peaks.py``

Function to detect peaks for stroke detection

``optimize_filter.py``

Optimization algorithms for Complementary and Kalman Filter tuning.

Data Munging Process
====================

The raw data collected from the smartphone and the differential GPS require a
set of operations to bring it into a form ready for the sensor fusion and
rowing metric calculations.

The smartphone GPS coordinates are converted to Cartesian coordinates using the
EPGS projection for the California Zone 2 local. The boat speed from the DGPS
is computed by numerically differentiating the Cartesian coordinates.

Times are synchronized based on the GPS satellite provided time stamps. All
signals are linearly interpolated with respect to the differential GPS time
stamps.

Differential GPS
----------------

The differential GPS data is converted to distance and speed time series
sampled at approximately 10 Hz for each trial and then synchronized in time to
the smartphone reported time.

1. Convert DGPS satellite seconds since GPS epoch to a date-time stamp.
2. Convert the DGPS satellite date-time stamp time to UNIX Epoch date-time by
   shifting 18 seconds to account for the accumulated leap seconds.
3. Calculate the cumulative distance traveled along the boat's path.
4. Compute the boat speed using backward differentiation.
5. Truncate each data file such that only the rowing or holding section in time
   is left.

Smartphone
----------

The smartphone data is collected in a continuously time series for all trials
in a single day.

1. Remove unused measurements (all but seconds since Unix Epoch, latitude,
   longitude, speed, and the Y component of the smartphone acceleration).
2. Separate the acceleration measurement from the other measurements.

GPS
----

The smartphone position (latitude and longitude) are converted to distance
measures at approximately 100 Hz.

1. Drop any duplicate time stamps.
2. Drop any rows that contain a single missing value.
3. Sort the rows by UNIX Epoch time.
4. Section the data into trials based on the trial start and stop times chosen
   for the DGPS data.
5.  Convert latitude and longitude to local Cartesian X (East-West) and Y
    (North-South) coordinates using EPGS California Zone 2 projection.
6.  Linearly interpolate the X and Y coordinates at the accelerometer
    measurement times.
7. Calculate the cumulative distance traveled along the boat's path from the
   interpolated X and Y coordinates.

Accelerometer
-------------

1. Drop any duplicate rows.
2. Drop any rows that contain missing values.
3. Sort the rows by GPS time.
4. Convert acceleration to units of m/s^2.

Filtering
---------

Complementary filter

1. Twice integrate the alpha_y acceleration measurement with respect to time
   using the trapezoidal rule.
2. High pass filter the twice integrated acceleration measurement.
3. Extrapolate the smartphone distance.
4. Low pass filter the extrapolated smartphone distance.
5. Sum the high and low pass filtered distance estimates.

Kalman filter

1. Run the optimize_R function to determine a general R matrix that minimizes
   the error across all trials.
2. Set up states matrix and measurement noise and process noise matrices.
3. Filter:

    1. Model prediction: estimate the states of the next time step using the
       model dynamics.
    2. Measurement update: Adjust the estimated value by accounting for the
       measurement values.

Metrics
-------

Slice each trial into strokes by using a peak detection algorithm to determine
the catch, which is the most definable peak in the y acceleration.

Steps to calculate error in distance per stroke estimates:

1. Start with distance estimates from: DGPS, SMGPS, KFILTER, CFILTER which are
   all synchronized in time.
2. Break every signal in step 1. from each trial into strokes based on the
   start and end times determined from the accelerometer.
3. For each stroke calculate distance traveled in meters by subtracting the
   stroke end distance from the stroke start distance.
4. Calculate the root mean square error for subsets of strokes.

Validation
----------

Smartphone measurement errors were estimated using the following method:

1. Interpolating DGPS timestamp to match phone.
2. Compute XY coordinates from lat/long using equi-rectangular projection w/
   small angle approximation.
3. Compute the distance between each phone measurement and each dgps
   measurement.
4. Compute the relative error of the phone against the dgps.
5. Find mean of phone error
