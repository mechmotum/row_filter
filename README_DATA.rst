Data Explanation
================

Smartphone GPS-derived longitude, latitude, and speed were collected with
timestamps via two smartphones at approximately 0.3 Hz. North-south and
east-west Cartesian positions relative to the differential GPS base station
were  collected at 10 Hz. Accelerometer and angular rate gyro data in the body
fixed Cartesian axes of the smartphone were collected from the boat and rower's
back at approximately 100 Hz. Video was collected at 720p, 60 frames per
second. Each set of timeseries from a single trial is also accompanied by
categorical metadata.

The compressed data file contains two directories, one for each day of
experiments with the two rowers. These directories each contain a directory for
the DGPS data and the smartphone data. There is a single ``csv`` file for each
trial in the DGPS directory and a single ``csv`` file for all of the trials in
the smartphone directory. There are two smartphone data files. One smartphone
was attached to the boat and the other to the small of the rower's back. The
diffGPS directories contain a ``csv`` file with the experimental log notes.

Directory layout::

   row_data.zip
   |
   ---> club-level
   |    |
   |    ---> diffGPS
   |    |    - 1club_experimental_log.csv
   |    |    - log1_baseline_log_date_*.csv
   |    |    - log1_positiion_log_date_*.csv
   |    |    - log1_velocity_log_date_*.csv
   |    |    .
   |    |    .
   |    |    .
   |    |    - log14_baseline_log_date_*.csv
   |    |    - log14_positiion_log_date_*.csv
   |    |    - log14_velocity_log_date_*.csv
   |    ---> iPhone
   |         - Boat2x-*.csv
   |         - Pelvis2x-*.csv
   |
   ---> elite
   |    |
   |    ---> diffGPS
   |    |    - 1elite_experimental_log.csv
   |    |    - baseline_log_date_*.csv
   |    |    .
   |    |    .
   |    |    .
   |    |    - baseline_log_date_*.csv
   |    ---> iPhone
   |         - Boat-*.csv
   |         - Waist-*.csv
   |

Metadata
--------

================== ================================
Variable           Possible Values
================== ================================
Rower              Club-level or Elite
Boat               Double or Single
Direction          Northwest or Southeast
Date               18/04/20 or 18/04/22
Time of day        9 AM - 11 AM
Target stroke rate hold, 16, 20, 22, 24, 26, 28, 34
================== ================================

"hold" means the rower attempted to stay still for the duration of the trial.

Additionally, the files ``1club_experimental_log.csv`` and
``1elite_experimental_log.csv`` contain these data columns with rows
corresponding to trials:

``Filename``
   Unique filename identifier corresponding to the DGPS files.
``Target_Stroke``
   Stroke rate label: 16, 20, 22, 24, 26, 28, 34, or hold.
``Direction``
   north or south, corresponding to northwest or southeast.
``Notes``
   Any notes taken during the trial.

The DGPS file ending in ``-094512.csv`` corresponds to a trial of us collecting
data while both the roving and stationary DGPS base stations were not moving on
solid ground.

DGPS data columns
-----------------

Following from: https://docs.swiftnav.com/wiki/Logging_Data_from_the_Console#Baseline
and also from: https://support.swiftnav.com/customer/en/portal/articles/2838278-swift-console-user-s-guide

``pc_time``
   System time and date, down to the microsecond in this format:
   ``%Y%m%d-%H%M%S``.
``gps_time``
   Full GPS time and date, down to the microsecond in this format:
   ``%Y%m%d-%H%M%S``.
``tow(sec)``
   Gives the number of whole seconds since the beginning of the week.
``north(meters)``
   The north distance between the base and the rover, in meters.
``east(meters)``
   The east distance between the base and the rover, in meters.
``down(meters)``
   The down distance (relative altitude) between the base and the rover, in
   meters.
``h_accuracy(meters)``
   Estimated horizontal accuracy, in meters.
``v_accuracy(meters)``
   Estimated vertical accuracy, in meters.
``distance(meters)``
   The total length of the NED vector between the base and rover, in meters.
``num_sats``
   The number of satellites that Piksi was tracking when this observation was
   recorded.
``flags``
   Describes which mode the RTK algorithm is in: 0x00 = float mode,
   0x01 = fixed mode.
``num_hypothesis``
   The number of fixed integer hypotheses that Piksi is considering at the
   moment.

iPhone data columns
-------------------

``session_id``
   Unique numerical id assigned to session (the recording between start and
   stop).
``log_time``
   The phone's recognized time (unix timestamp).
``device_orientation``
   Apple's code for how the device is attitudinally oriented. Portrait,
   landscape, left, right, etc. all represented as a number.

   - 0=unknown
   - 1=portrait
   - 2=portraitUpsidedown
   - 3=landscapeLeft
   - 4=landscapeRight
   - 5=faceUp
   - 6=faceDown

GPS related data
~~~~~~~~~~~~~~~~

Following from: https://developer.apple.com/documentation/corelocation/cllocation

``location_timestamp_since_1970``
   The time at which this location was determined.
``location_latitude``, ``location_longitude``
   The geographical coordinate information.
``location_altitude``
   The altitude, measured in meters.
``location_vertical_accuracy``
   The accuracy of the altitude value, measured in meters.
``location_horizontal_accuracy``
   The radius of uncertainty for the location, measured in meters.
``location_speed``
   The instantaneous speed of the device, measured in meters per second.
``location_course``
   The direction in which the device is traveling, measured in degrees and
   relative to due north.

Following from: https://developer.apple.com/documentation/corelocation/clheading

``location_heading_timestamp_since_1970``
   The time at which this heading was determined.
``location_heading_x``, ``location_heading_y``, ``location_heading_z``
   The geomagnetic data (measured in microteslas) for the x-axis, y-axis, and
   z-axis.
``location_true_heading``
   The heading (measured in degrees) relative to true north.
``location_magnetic_heading``
   The heading (measured in degrees) relative to magnetic north.
``location_heading_accuracy``
   The maximum deviation (measured in degrees) between the reported heading and
   the true geomagnetic heading.

Raw Sensor Measurements
~~~~~~~~~~~~~~~~~~~~~~~

Following from: https://developer.apple.com/documentation/coremotion/cmacceleration

``accelerometer_timestamp_since_reboot``
   NA
``accelerometer_acceleration_x``, ``accelerometer_acceleration_y``, ``accelerometer_acceleration_z``
   The values reported by the accelerometers are measured in increments of the
   gravitational acceleration, with the value 1.0 representing an acceleration
   of 9.8 meters per second (per second) in the given direction.

Following from: https://developer.apple.com/documentation/coremotion/cmgyrodata

``gyro_timestamp_since_reboot``
   NA
``gyro_rotation_x``, ``gyro_rotation_y``, ``gyro_rotation_z``
   Retrieve data from the onboard gyroscopes. Rotation values are measured in
   radians per second around the given axis.

Following from: https://developer.apple.com/documentation/coremotion/cmaltitudedata

``altimeter_timestamp_since_reboot``
   NA
``altimeter_relative_altitude``, ``altimeter_pressure``
   Provide altitude data based on barometric sensor information. The change in
   altitude (in meters) since the first reported event. The recorded pressure,
   in kilopascals.

Internal iPhone Filter Estimates
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

   Deliver acceleration, attitude, rotation, and magnetic field data that is
   adjusted for gravity and other forms of bias.

Following from: https://developer.apple.com/documentation/coremotion/cmattitude

``motion_timestamp_since_reboot``
   NA
``motion_yaw``, ``motion_roll`` ``motion_pitch``
   The attitude of the device. The roll of the device, in radians. The pitch of
   the device, in radians. The yaw of the device, in radians.
``motion_quaternion_x``, ``motion_quaternion_y``, ``motion_quaternion_z``, ``motion_quaternion_w``
   Returns a quaternion representing the device's attitude.

Following from: https://developer.apple.com/documentation/coremotion/cmdevicemotion

``motion_rotation_rate_x``, ``motion_rotation_rate_y``, ``motion_rotation_rate_z``,
   The rotation rate of the device. The value of this property contains a
   measurement of gyroscope data whose bias has been removed by Core Motion
   algorithms.
``motion_user_acceleration_x``, ``motion_user_acceleration_y``, ``motion_user_acceleration_z``
   The acceleration that the user is giving to the device.
``motion_gravity_x``, ``motion_gravity_y``, ``motion_gravity_z``
   The gravity acceleration vector expressed in the device's reference frame.
``motion_magnetic_field_x``, ``motion_magnetic_field_y``, ``motion_magnetic_field_z``
   Returns the magnetic field vector with respect to the device.
``motion_magnetic_field_calibration_accuracy``
   Indicates the calibration accuracy of a magnetic field estimate
