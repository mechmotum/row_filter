import os
from collections import defaultdict

import yaml
import numpy as np
from scipy.optimize import leastsq
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from filterpy.kalman import KalmanFilter

from row_filter.clean_data import clean_data

# DGPS : differential GPS
# SP : smartphone

DATA_DIR = 'notebooks/row_data'
FIG_DIR = 'figures'
ELITE_SP_FNAME = ('Boat-20180422T103229_1641_rpc364_data_1CLX_1_B_2CDF0487-'
                  '83FC-45CC-B590-FF42D74E0D6D.csv')
CLUB_SP_FNAME = ('Boat2x-20180420T085713_1633_rpc364_data_1CLX_1_B_F92041BC-'
                 '2503-4150-8196-2B45C0258ED8.csv')

with open('metadata.yml') as f:
    metadata = yaml.load(f)

df_sp = {'club': pd.read_csv(os.path.join(DATA_DIR, 'amateur', 'iPhone',
                                          CLUB_SP_FNAME)),
         'elite': pd.read_csv(os.path.join(DATA_DIR, 'elite', 'iPhone',
                                           ELITE_SP_FNAME))}


def run_filter(q, r, meas):
    """

    Parameters
    ==========
    q : array_like, shape(3, 3)
        The process noise covariance matrix.
    r : array_like, shape(2, 2)
        The measurement noise covariance matrix.
    meas : array_like, shape(4, n)
        The time, distance, speed, and acceleration measurement time series.

    """

    filt = KalmanFilter(dim_x=3, dim_z=2, dim_u=1)

    filt.Q = q
    filt.R = r

    t, d, v, a = meas

    # Initializes the filter's states with the first measurements of distance
    # and speed. The bias initial value is chosen as an average of what the
    # bias seems to converge to for all trials.
    filt.x = np.array([[d[0]],
                       [v[0]],
                       [0.42]])

    # The state transition and input matrices change each update because of the
    # variable time change between updates.

    dt = np.diff(t)
    dt = np.hstack((dt[0], dt))

    Fs = np.tile(np.eye(3), (len(t), 1, 1))
    Fs[:, 0, 1] = dt
    Fs[:, 1, 2] = -dt

    B = np.zeros((3, 1))
    Bs = np.tile(B, (len(t), 1, 1))
    Bs[:, 1, 0] = dt

    # The measurements are simply distance and speed.
    filt.H = np.array([[1, 0, 0],
                       [0, 1, 0]])

    # Guesses at the variance of the initial states.
    filt.P = np.diag([1.0**1, 1.0**2, 0.5**2])

    measurements = np.vstack((d, v))
    measurements = [m.reshape(2, 1) for m in measurements.T]

    estimates = []
    covariances = np.nan * np.ones((len(t), 3, 3))
    kalman_gains = np.nan * np.ones((len(t), 3, 2))

    for i, ti in enumerate(t):

        filt.predict(u=a[i], B=Bs[i], F=Fs[i])

        if UPDATE_MEAS_WITH_GPS:
            if i in meas_update_idxs:
                filt.update(measurements[i])
            else:
                filt.update(None)
        else:
            filt.update(measurements[i])

        estimates.append(filt.x)
        covariances[i] = filt.P
        kalman_gains[i] = filt.K

    estimates = np.array(estimates).squeeze().T
    kalman_gains = np.array(kalman_gains)

    return estimates, covariances, kalman_gains


def parse_qr(qr):

    q = qr[:-3]

    rd, rv, rdv = qr[-3:]

    # The optimization will find negtive values for the diagonals of the Q and
    # R matrices sometimes, which should not be allowed. The abs() is a hack to
    # force them to always be postive.

    r = np.array([[np.abs(rd), rdv],
                  [rdv, np.abs(rv)]])

    if len(q) == 1:  # process noise on bias state
        q = np.diag([0.0, 0.0, q[0]])
    if len(q) == 2:  # process noise on speed and bias state
        q = np.diag([0.0, q[0], q[1]])
    elif len(q) == 3:  # process noise on all states
        q = np.diag(q)
    else:  # full covariance matrix
        q = np.array([[np.abs(q[0]), q[3], q[4]],
                      [q[3], np.abs(q[1]), q[5]],
                      [q[4], q[5], np.abs(q[2])]])

    return q, r


def error(qr, meas, standard):
    """Returns an array of distance errors.

    qr : array_like, shape(n + 3

    qr = [qphi, rd, rv, rdv]
    qr = [qv, qphi, rd, rv, rdv]
    qr = [qd, qv, qphi, rd, rv, rdv]
    qr = [qd, qv, qphi, qdv, qdphi, qvphi, rd, rv, rdv]

    """
    estimates, _, _ = run_filter(*parse_qr(qr), meas)[0]
    return (standard[0] - estimates).flatten()


def calc_acc_and_prec(truth, estimate):
    """Returns the central error, standard deviation of the error, and the root
    mean square error."""
    error = estimate - truth
    central_error = np.abs(np.mean(error))
    std_error = np.std(error)
    rmse = np.sqrt(np.mean(error**2))
    return central_error, std_error, rmse


def plot_distance(ax, time, dgps_dist, sp_dist, kf_dist):
    """Plot distance versus time comparisons."""
    ax.plot(time, dgps_dist, label='DGPS', color='black')
    ax.plot(time, sp_dist, label='SP')
    ax.plot(time, kf_dist, label='KF')
    ax.set_ylabel('Distance [m]')
    ax.legend()


def plot_speed(ax, time, dgps_speed, sp_speed, kf_speed):
    """Plot speedance versus time comparisons."""
    ax.plot(time, dgps_speed, label='DGPS', color='black')
    ax.plot(time, sp_speed, label='SP')
    ax.plot(time, kf_speed, label='KF')
    ax.set_ylabel('Speed [m/s]')
    ax.legend()


def plot_error(ax, typ, time, dgps, sp, kf):

    sp_error = sp - dgps
    kf_error = kf - dgps

    lab = 'SP CE {:1.2f}, STD {:1.2f}, RMSE {:1.2f}'
    ax.plot(time, sp_error, label=lab.format(*calc_acc_and_prec(dgps, sp)))

    lab = 'SP CE {:1.2f}, STD {:1.2f}, RMSE {:1.2f}'
    ax.plot(time, kf_error, label=lab.format(*calc_acc_and_prec(dgps, kf)))

    ax.set_ylabel('{} Error [m/s]'.format(typ))
    ax.legend()


def plot_kalman_cov(ax, time, covs):

    ax.set_title('Kalman Covariance')
    ax.plot(time, covs[:, 0, 0],
            label='dd, final val={:1.3f}'.format(covs[-1, 0, 0]))
    ax.plot(time, covs[:, 0, 1],
            label='dv, final val={:1.3f}'.format(covs[-1, 0, 1]))
    ax.plot(time, covs[:, 0, 2],
            label=r'd$\phi$, final val={:1.3f}'.format(covs[-1, 0, 2]))
    ax.plot(time, covs[:, 1, 1],
            label='vv, final val={:1.3f}'.format(covs[-1, 1, 1]))
    ax.plot(time, covs[:, 1, 2],
            label=r'v$\phi$, final val={:1.3f}'.format(covs[-1, 1, 2]))
    ax.plot(time, covs[:, 2, 2],
            label=r'$\phi\phi$, final val={:1.3f}'.format(covs[-1, 2, 2]))
    ax.legend()


def plot_kalman_gain(ax, time, gains):

    # Kalman Gain is a 3 x 2
    ax.set_title('Kalman Gain')
    ax.plot(time, gains[:, 0, 0],
            label='dd, final val={:1.3f}'.format(gains[-1, 0, 0]))
    ax.plot(time, gains[:, 0, 1],
            label='dv, final val={:1.3f}'.format(gains[-1, 0, 1]))
    ax.plot(time, gains[:, 1, 0],
            label='vd, final val={:1.3f}'.format(gains[-1, 1, 0]))
    ax.plot(time, gains[:, 1, 1],
            label='vv, final val={:1.3f}'.format(gains[-1, 1, 1]))
    ax.plot(time, gains[:, 2, 0],
            label=r'$\phi$d, final val={:1.3f}'.format(gains[-1, 2, 0]))
    ax.plot(time, gains[:, 2, 1],
            label=r'$\phi$v, final val={:1.3f}'.format(gains[-1, 2, 1]))
    ax.legend()
    ax.set_xlabel('Time [s]')


def plot_time_series(metadata, fname, df_dgps_clean, df_sp_clean, kf_res, covs,
                     gains, q_sol, r_sol):

    fig, axes = plt.subplots(7, 1, sharex=True, figsize=(12, 20))

    m = metadata

    title = 'Kalman Filter Distance and Speed Comparisons\n{}\n{}\nQ={}\nR={}'
    axes[0].set_title(title.format(m['rower'] + ' ' + str(m['stroke_rate']) +
                                   m['direction'], fname, q_sol, r_sol))

    time = df_sp_clean.log_time - df_sp_clean.log_time[0]

    plot_distance(axes[0], time, df_dgps_clean['pos'], df_sp_clean['y_boat'],
                  res[0])

    plot_error(axes[1], 'Distance', time, df_dgps_clean['pos'],
               df_sp_clean['y_boat'], res[0])

    plot_speed(axes[2], time, df_dgps_clean['speed'],
               df_sp_clean['location_speed'], res[1])

    plot_error(axes[3], 'Speed', time, df_dgps_clean['speed'],
               df_sp_clean['location_speed'], res[1])

    # bias versus time
    lab = 'KF, Final value={:1.2f}'.format(res[2][-1])
    axes[4].plot(time, res[2], label=lab, color=axes[3].lines[1].get_color())
    axes[4].set_ylabel(r'Bias [$m/s^2$]')
    axes[4].legend()

    plot_kalman_cov(axes[5], time, covs)

    plot_kalman_gain(axes[6], time, gains)

    return fig


def save_figure(fig, metadata):

    m = metadata

    if UPDATE_MEAS_WITH_GPS:
        meas_title = '-infrequent'
    else:
        meas_title = ''

    fig_fname = ('jasons-kalman-' +
                 '-'.join([m['boat'], m['rower'], str(m['stroke_rate']),
                           m['direction']]) + meas_title + '.png')

    if not os.path.exists(FIG_DIR):
        os.makedirs(FIG_DIR)

    fig.savefig(os.path.join(FIG_DIR, fig_fname))


def fix_dates(df_dgps, metadata):
    # TODO : This belongs in clean_data().
    df_dgps.pc_time = (metadata['date'] + ' ' + metadata['pc_hour'] +
                       ':' + df_dgps.pc_time)
    df_dgps.gps_time = (metadata['date'] + ' ' + metadata['gps_hour'] +
                        ':' + df_dgps.gps_time)
    return df_dgps


def extract_sp_update_idxs(df_sp_clean):
    # The smartphone GPS distance and speed measurements only update every 3
    # seconds or so. This determines the indices in the measurement arrays that
    # correspond to GPS update times.
    meas_update_idxs = df_sp_clean[df_sp_clean['y_boat'].diff() > 0.0].log_time.index.values
    meas_update_idxs = np.hstack((df_sp_clean.index.values[0], meas_update_idxs))
    return meas_update_idxs


def find_optimal_filter(df_dgps_clean, df_sp_clean, df_acc_clean):

    meas = np.vstack((df_sp_clean['log_time'].values,
                      df_sp_clean['y_boat'].values,
                      df_sp_clean['location_speed'].values,
                      df_acc_clean['ay_boat'].values))

    standard = np.vstack((df_dgps_clean['pos'], df_dgps_clean['speed']))

    # q_sol = np.array([1213.82, 0, 0, 0, 22.41, 0, 0, 0, 0.15]) / 1000000000
    # q_sol = np.array([22.41, 0.15]) / 1000000000
    # q_sol = np.array([1213.82, 22.41, 0.15]) / 1000000000
    q_sol = np.zeros(3)

    # NOTE : These all seem to make the speed fit worse.
    #filt.R = np.array([[12.80597902,  6.14671811],
                       #[ 6.14671811, 23.28685825]])
    #filt.R = np.array([[ 3.08219391, -0.28343086],
                       #[-0.28343086,  7.13911067]])
    #filt.R = np.array([[2.17498954, 0.64872011],
                       #[0.64872011, 3.28449025]])

    r_sol = np.array([1029.40447394, 9.45307171,  97.28192111])
    r_sol = np.array([100.0, 10.0, 1.0])
    #r_sol = np.array([14.823, 0.624, 0.0])
    qr_sol = np.hstack((q_sol, r_sol))

    #qr_sol, _ = leastsq(error, qr_sol, args=(meas, standard))

    q_sol, r_sol = parse_qr(qr_sol)

    print('Q =', q_sol)
    print('R =', r_sol)

    return q_sol, r_sol, meas


def collect_summary_data(summary_data, fname, metadata):
    # rows: each trial
    # columns: all metadata
    # total central error
    # total standard deivation of error
    # total rmse
    # last half central error
    # last half standard deivation of error
    # last half rmse
    # final bias

    summary_data['filename'].append(fname)

    for k, v in m.items():
        summary_data[k].append(v)

    start = int(len(df_sp_clean) / 2)

    ce, std, rmse = calc_acc_and_prec(df_dgps_clean['pos'].iloc[-start:],
                                      df_sp_clean['y_boat'].iloc[-start:])
    summary_data['sp_dist_ce'].append(ce)
    summary_data['sp_dist_std'].append(std)
    summary_data['sp_dist_rmse'].append(rmse)

    ce, std, rmse = calc_acc_and_prec(df_dgps_clean['speed'].iloc[-start:],
                                      df_sp_clean['location_speed'].iloc[-start:])
    summary_data['sp_speed_ce'].append(ce)
    summary_data['sp_speed_std'].append(std)
    summary_data['sp_speed_rmse'].append(rmse)

    ce, std, rmse = calc_acc_and_prec(df_dgps_clean['pos'].iloc[-start:],
                                      res[0, -start:])
    summary_data['kf_dist_ce'].append(ce)
    summary_data['kf_dist_std'].append(std)
    summary_data['kf_dist_rmse'].append(rmse)

    ce, std, rmse = calc_acc_and_prec(df_dgps_clean['speed'].iloc[-start:],
                                      res[1, -start:])
    summary_data['kf_speed_ce'].append(ce)
    summary_data['kf_speed_std'].append(std)
    summary_data['kf_speed_rmse'].append(rmse)

    summary_data['kf_bias'].append(np.mean(res[2, -500:]))


def melt_summary_data(summary_data):

    # not every trial has this data, so delete
    del summary_data['pc_hour']
    del summary_data['gps_hour']

    summary_data = pd.DataFrame(summary_data)

    dat_cols = [c for c in summary_data.columns
                if c.startswith('sp_') or c.startswith('kf_') and
                'bias' not in c]
    df = pd.melt(summary_data, id_vars=['filename', 'rower', 'stroke_rate',
                                        'direction'], value_vars=dat_cols)
    df['estimator'] = df['variable'].str.split('_', expand=True)[0]
    df['measure'] = df['variable'].str.split('_', expand=True)[1]
    df['statistic'] = df['variable'].str.split('_', expand=True)[2]

    del df['variable']

    return df

summary_data = defaultdict(list)


for UPDATE_MEAS_WITH_GPS in [True, False]:
    for fname in sorted(metadata.keys()):

        print(fname)

        m = metadata[fname]

        # TODO : Rename data directory from amateur to club.
        rower = 'amateur' if m['rower'] == 'club' else 'elite'

        df_dgps = pd.read_csv(os.path.join(DATA_DIR, rower, 'diffGPS', fname))

        # clean and prepare the data
        if m['prepend_date_hour']:
            df_dgps = fix_dates(df_dgps, m)

        df_sp_clean, df_acc_clean, df_dgps_clean = clean_data(df_sp[m['rower']],
                                                            df_dgps)
        # TODO : There is a nan in the first speed entry. Fix this in the row_filter
        # lib.
        df_dgps_clean['speed'].iloc[0] = df_dgps_clean['speed'].iloc[1]

        meas_update_idxs = extract_sp_update_idxs(df_sp_clean)

        # run the [optimal] filter
        q_sol, r_sol, meas = find_optimal_filter(df_dgps_clean, df_sp_clean,
                                                df_acc_clean)

        res, covs, gains = run_filter(q_sol, r_sol, meas)

        # plot the results and save the figure
        fig = plot_time_series(m, fname, df_dgps_clean, df_sp_clean, res, covs,
                            gains, q_sol, r_sol)

        save_figure(fig, m)

        plt.close(fig)

        collect_summary_data(summary_data, fname, m)

    df = melt_summary_data(summary_data)
    sns.catplot(x='statistic', y='value', hue='estimator', col='measure',
            kind='box', sharey=False, data=df)
