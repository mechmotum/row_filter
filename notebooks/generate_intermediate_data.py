# Generates:
    # norm_stroke_accel.csv, 
    # norm_stroke_speed.csv,
    # all_stroke.csv

from row_filter import clean_data_norm as cdn
from row_filter import clean_data_stroke_accy as cds_accy
from row_filter import clean_data as cd
import pandas as pd

print('Loading Elite Data...')

# Single Scull DGPS
df_dgps1 = pd.read_csv("row_data/elite/diffGPS/baseline_log_20180422-104956.csv") #16N 
df_dgps2 = pd.read_csv("row_data/elite/diffGPS/baseline_log_20180422-104427.csv") #16S
df_dgps3 = pd.read_csv("row_data/elite/diffGPS/baseline_log_20180422-105906.csv") #20N
df_dgps4 = pd.read_csv("row_data/elite/diffGPS/baseline_log_20180422-105358.csv") #20S
df_dgps5 = pd.read_csv("row_data/elite/diffGPS/baseline_log_20180422-110625.csv") #24N 
df_dgps6 = pd.read_csv("row_data/elite/diffGPS/baseline_log_20180422-110303.csv") #24S
df_dgps7 = pd.read_csv("row_data/elite/diffGPS/baseline_log_20180422-111307.csv") #28N
df_dgps8 = pd.read_csv("row_data/elite/diffGPS/baseline_log_20180422-110951.csv") #28S
df_dgps9 = pd.read_csv("row_data/elite/diffGPS/baseline_log_20180422-111703.csv") #34S 

#Time issues 
df_dgps1.pc_time = ["2018-04-22 10:" + item for item in df_dgps1.pc_time]
df_dgps1.gps_time = ["2018-04-22 17:" + item for item in df_dgps1.gps_time]
df_dgps5.pc_time = ["2018-04-22 11:" + item for item in df_dgps5.pc_time]
df_dgps5.gps_time = ["2018-04-22 18:" + item for item in df_dgps5.gps_time]
df_dgps9.pc_time = ["2018-04-22 11:" + item for item in df_dgps9.pc_time]
df_dgps9.gps_time = ["2018-04-22 18:" + item for item in df_dgps9.gps_time]

#Single Scull Phone
df_phone = pd.read_csv("row_data/elite/iPhone/Boat-20180422T103229_1641_rpc364_data_1CLX_1_B_2CDF0487-83FC-45CC-B590-FF42D74E0D6D.csv")
df_waist = pd.read_csv("row_data/elite/iPhone/Waist-20180422T103407_1639_smweil123_data_1CLX_1_W_D066BBC7-967D-4CE8-854C-E9A2FCCEAFF6.csv")

print('Loading Club Data...')

#Double Scull DGPS
df2_dgps1 = pd.read_csv("row_data/club-level/diffGPS/log1_baseline_log_20180420-090601.csv") #20N
df2_dgps2 = pd.read_csv("row_data/club-level/diffGPS/log4_baseline_log_20180420-091604.csv") #22N
df2_dgps3 = pd.read_csv("row_data/club-level/diffGPS/log2_baseline_log_20180420-091022.csv") #22S   #issue
df2_dgps4 = pd.read_csv("row_data/club-level/diffGPS/log8_baseline_log_20180420-092443.csv") #24N
df2_dgps5 = pd.read_csv("row_data/club-level/diffGPS/log6_baseline_log_20180420-092004.csv") #24S
df2_dgps6 = pd.read_csv("row_data/club-level/diffGPS/log12_baseline_log_20180420-093324.csv") #26N
df2_dgps7 = pd.read_csv("row_data/club-level/diffGPS/log10_baseline_log_20180420-092925.csv") #26S

#Time Issues
df2_dgps3.pc_time = ["2018-04-20 09:" + item for item in df2_dgps3.pc_time]
df2_dgps3.gps_time = ["2018-04-20 16:" + item for item in df2_dgps3.gps_time]
#Double Scull Phone
df2_phone = pd.read_csv("row_data/club-level/iPhone/Boat2x-20180420T085713_1633_rpc364_data_1CLX_1_B_F92041BC-2503-4150-8196-2B45C0258ED8.csv")
df_waist2 = pd.read_csv("row_data/club-level/iPhone/Pelvis2x-20180420T085631_1634_mdirrim_data_ERG_EE3C0E5C-D2E2-4398-9DA4-A8C7D6701F3C.csv")

print('Cleaning Data...')
# General Data Cleaning:
df_phone1, df_acc1, df_dgps1 = cd.clean_data(df_phone, df_dgps1)
df_phone2, df_acc2, df_dgps2 = cd.clean_data(df_phone, df_dgps2)
df_phone3, df_acc3, df_dgps3 = cd.clean_data(df_phone, df_dgps3)
df_phone4, df_acc4, df_dgps4 = cd.clean_data(df_phone, df_dgps4)
df_phone5, df_acc5, df_dgps5 = cd.clean_data(df_phone, df_dgps5)
df_phone6, df_acc6, df_dgps6 = cd.clean_data(df_phone, df_dgps6)
df_phone7, df_acc7, df_dgps7 = cd.clean_data(df_phone, df_dgps7)
df_phone8, df_acc8, df_dgps8 = cd.clean_data(df_phone, df_dgps8)
df_phone9, df_acc9, df_dgps9 = cd.clean_data(df_phone, df_dgps9)

df2_phone1, df2_acc1, df2_dgps1 = cd.clean_data(df2_phone, df2_dgps1)
df2_phone2, df2_acc2, df2_dgps2 = cd.clean_data(df2_phone, df2_dgps2)
df2_phone3, df2_acc3, df2_dgps3 = cd.clean_data(df2_phone, df2_dgps3)
df2_phone4, df2_acc4, df2_dgps4 = cd.clean_data(df2_phone, df2_dgps4)
df2_phone5, df2_acc5, df2_dgps5 = cd.clean_data(df2_phone, df2_dgps5)
df2_phone6, df2_acc6, df2_dgps6 = cd.clean_data(df2_phone, df2_dgps6)
df2_phone7, df2_acc7, df2_dgps7 = cd.clean_data(df2_phone, df2_dgps7)

print('Clean data stroke...')
# Clean Data for Stroke Detection: Single
df_stroke1 = cds_accy.clean_data(df_phone1, df_dgps1, df_acc1, '20180422-104956') #16N
df_stroke2 = cds_accy.clean_data(df_phone2, df_dgps2, df_acc2, '20180422-104427') #16S
df_stroke3 = cds_accy.clean_data(df_phone3, df_dgps3, df_acc3, '20180422-105906') #20N
df_stroke4 = cds_accy.clean_data(df_phone4, df_dgps4, df_acc4, '20180422-105358') #20S
df_stroke5 = cds_accy.clean_data(df_phone5, df_dgps5, df_acc5, '20180422-110625') #24N
df_stroke6 = cds_accy.clean_data(df_phone6, df_dgps6, df_acc6, '20180422-110303') #24S
df_stroke7 = cds_accy.clean_data(df_phone7, df_dgps7, df_acc7, '20180422-111307') #28N
df_stroke8 = cds_accy.clean_data(df_phone8, df_dgps8, df_acc8, '20180422-110951') #28S
df_stroke9 = cds_accy.clean_data(df_phone9, df_dgps9, df_acc9, '20180422-111703') #34S

# Clean Data for Stroke Detection: Double
df2_stroke1 = cds_accy.clean_data(df2_phone1, df2_dgps1, df2_acc1, '20180420-090601') #20N
df2_stroke2 = cds_accy.clean_data(df2_phone2, df2_dgps2, df2_acc2, '20180420-091604') #22N
df2_stroke3 = cds_accy.clean_data(df2_phone3, df2_dgps3, df2_acc3, '20180420-091022') #22S
df2_stroke4 = cds_accy.clean_data(df2_phone4, df2_dgps4, df2_acc4, '20180420-092443') #24N
df2_stroke5 = cds_accy.clean_data(df2_phone5, df2_dgps5, df2_acc5, '20180420-092004') #24S
df2_stroke6 = cds_accy.clean_data(df2_phone6, df2_dgps6, df2_acc6, '20180420-093324') #26N
df2_stroke7 = cds_accy.clean_data(df2_phone7, df2_dgps7, df2_acc7, '20180420-092925') #26S

print('Normalization Data cleaning...')
# Clean Data for Acceleration: Single
df_norm_acc1, df_norm_speed1 = cdn.clean_data(df_dgps1, df_stroke1, df_acc1, '20180422-104956') #16N
df_norm_acc2, df_norm_speed2 = cdn.clean_data(df_dgps2, df_stroke2, df_acc2,'20180422-104427') #16S
df_norm_acc3, df_norm_speed3 = cdn.clean_data(df_dgps3, df_stroke3, df_acc3,'20180422-105906') #20N
df_norm_acc4, df_norm_speed4 = cdn.clean_data(df_dgps4, df_stroke4, df_acc4,'20180422-105358') #20S
df_norm_acc5, df_norm_speed5 = cdn.clean_data(df_dgps5, df_stroke5, df_acc5,'20180422-110625') #24N
df_norm_acc6, df_norm_speed6 = cdn.clean_data(df_dgps6, df_stroke6, df_acc6,'20180422-110303') #24S
df_norm_acc7, df_norm_speed7 = cdn.clean_data(df_dgps7, df_stroke7, df_acc7,'20180422-111307') #28N
#df_stroke8 = cdn.clean_data(df_phone8, df_dgps8, df_acc8,'20180422-110951') #28S
df_norm_acc9, df_norm_speed9 = cdn.clean_data(df_dgps9, df_stroke9, df_acc9,'20180422-111703') #34S

# Clean Data for Acceleration: Double
df2_norm_acc1, df2_norm_speed1 = cdn.clean_data(df2_dgps1, df2_stroke1, df2_acc1, '20180420-090601') 
df2_norm_acc2, df2_norm_speed2 = cdn.clean_data(df2_dgps2, df2_stroke2, df2_acc2,'20180420-091604') 
df2_norm_acc3, df2_norm_speed3 = cdn.clean_data(df2_dgps3, df2_stroke3, df2_acc3,'20180420-091022') 
df2_norm_acc4, df2_norm_speed4 = cdn.clean_data(df2_dgps4, df2_stroke4, df2_acc4,'20180420-092443')
df2_norm_acc5, df2_norm_speed5 = cdn.clean_data(df2_dgps5, df2_stroke5, df2_acc5,'20180420-092004') 
df2_norm_acc6, df2_norm_speed6 = cdn.clean_data(df2_dgps6, df2_stroke6, df2_acc6,'20180420-093324') 
df2_norm_acc7, df2_norm_speed7 = cdn.clean_data(df2_dgps7, df2_stroke7, df2_acc7,'20180420-092925') 

print('Building csvs...')
frames = [df_norm_acc1, df_norm_acc2, df_norm_acc3, df_norm_acc4, df_norm_acc5, df_norm_acc6,\
         df_norm_acc7, df_norm_acc9, df2_norm_acc1, df2_norm_acc2, df2_norm_acc3, df2_norm_acc4,\
         df2_norm_acc5, df2_norm_acc6, df2_norm_acc7]
df_norm_acc = pd.concat(frames, ignore_index=True)
df_norm_acc = cdn.create_norm_df(df_norm_acc)
filepath = 'row_data/norm_stroke_accel.csv'
df_norm_acc.to_csv(filepath)

frames = [df_norm_speed1, df_norm_speed2, df_norm_speed3, df_norm_speed4, \
          df_norm_speed5, df_norm_speed6, df_norm_speed7, df_norm_speed9, \
          df2_norm_speed1, df2_norm_speed2, df2_norm_speed3, \
          df2_norm_speed4, df2_norm_speed5, df2_norm_speed6, df2_norm_speed7]

df_norm_speed = pd.concat(frames, ignore_index=True)
df_norm_speed = cdn.create_norm_df(df_norm_speed)
filepath = 'row_data/norm_stroke_speed.csv'
df_norm_speed.to_csv(filepath)

frames = [df_stroke1, df_stroke2, df_stroke3, df_stroke4, df_stroke5, df_stroke6,\
          df_stroke7, df_stroke8, df_stroke9, df2_stroke1, df2_stroke2,\
          df2_stroke3, df2_stroke4, df2_stroke5, df2_stroke6, df2_stroke7]

all_stroke = pd.concat(frames, ignore_index=True)
filepath = 'row_data/all_stroke.csv'
df_stroke, df_dgpsinfo = cds_accy.create_stroke_df(all_stroke)
df_stroke.to_csv(filepath)
print('Done!')