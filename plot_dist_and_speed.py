"""This script plots the various estimates of speed from the smartphone and
DGPS data for a single trial for comparison."""

# TODO : Move this into a `bin` or `scripts` directory. We will need to
# implement something to handle the paths to the data first to do so.

import os

import yaml
import numpy as np
from scipy.interpolate import interp1d
import pandas as pd
import matplotlib.pyplot as plt
from dtk.process import derivative

from row_filter.clean_data import clean_data

# DGPS : differential GPS
# SP : smartphone

DATA_DIR = 'notebooks/row_data'
FIG_DIR = 'figures'
ELITE_SP_FNAME = ('Boat-20180422T103229_1641_rpc364_data_1CLX_1_B_2CDF0487-'
                  '83FC-45CC-B590-FF42D74E0D6D.csv')
CLUB_SP_FNAME = ('Boat2x-20180420T085713_1633_rpc364_data_1CLX_1_B_F92041BC-'
                 '2503-4150-8196-2B45C0258ED8.csv')


def plot_heading():

    cols = [
        'motion_yaw',
        'location_course',
        'location_heading_x',
        'location_heading_y',
        'location_heading_z',
        'location_true_heading',
        'location_magnetic_heading',
    ]

    axes = df_sp_clean.plot('log_time', cols, subplots=True, figsize=(16, 10))

    title = 'Heading\n{}\n{}'
    axes[0].set_title(title.format(m['rower'] + ' ' + str(m['stroke_rate']) +
                                   m['direction'], fname))

    fig = plt.gcf()

    fig_fname = 'heading-' + fig_fname_end

    if not os.path.exists(FIG_DIR):
        os.makedirs(FIG_DIR)

    fig.savefig(os.path.join(FIG_DIR, fig_fname))
    plt.close(fig)


def plot_path():
    """Generates a plot of the path of the boat in the XY plane according to
    the smartphone GPS and the DGPS for each trial. Also shows the error, mean
    error, and standard deviation of the error."""

    fig, axes = plt.subplots(1, 2, figsize=(12, 6))

    # Set first DGPS sample to X=0, Y=0
    dgps_east = -(df_dgps_clean['east(meters)'] -
                  df_dgps_clean['east(meters)'].iloc[0])
    dgps_north = -(df_dgps_clean['north(meters)'] -
                   df_dgps_clean['north(meters)'].iloc[0])

    # Downsample to the GPS ~0.3 Hz
    df_sp_red = df_sp_clean.drop_duplicates(subset='location_latitude')

    # Set first SP GPS sample to X=0, Y=0
    sp_east = df_sp_red['east'] - df_sp_red['east'].iloc[0]
    sp_north = df_sp_red['north'] - df_sp_red['north'].iloc[0]

    # Plot the path of each sensor on the water's surface
    axes[0].plot(dgps_east, dgps_north)
    axes[0].plot(sp_east, sp_north, '.')
    dgps_lab = 'DGPS, final time: {:1.2f} s'.format(
        df_dgps_clean.unix_time.iloc[-1] - df_dgps_clean.unix_time.iloc[0])
    sp_lab = 'SP, final time: {:1.2f} s'.format(
        df_sp_red.log_time.iloc[-1] - df_sp_red.log_time.iloc[0])
    axes[0].legend([dgps_lab, sp_lab])
    axes[0].set_xlabel('East')
    axes[0].set_ylabel('North')
    title = 'Path Comparisons\n{}\n{}'
    axes[0].set_title(title.format(m['rower'] + ' ' + str(m['stroke_rate']) +
                                   m['direction'], fname))
    axes[0].set_aspect('equal')

    # Calculate the error of each SP GPS position relative to the DGPS
    # (interpolated) value at the same time.
    int_east = interp1d(df_dgps_clean.unix_time, dgps_east)
    int_north = interp1d(df_dgps_clean.unix_time, dgps_north)
    error_east = sp_east - int_east(df_sp_red.log_time)
    error_north = sp_north - int_north(df_sp_red.log_time)

    # TODO : Store this covariance matrix to use as R in the Kalman filter.
    cov_mat = np.cov(error_east, y=error_north)
    cov_matrices.append(cov_mat)

    print(cov_mat)

    axes[1].axvspan(np.mean(error_east) - 3 * np.std(error_east),
                    np.mean(error_east) + 3 * np.std(error_east),
                    alpha=0.25, color='red')
    axes[1].axhspan(np.mean(error_north) - 3 * np.std(error_north),
                    np.mean(error_north) + 3 * np.std(error_north),
                    alpha=0.25, color='red')
    axes[1].axvline(np.mean(error_east), color='red')
    axes[1].axhline(np.mean(error_north), color='red')
    axes[1].plot(error_east, error_north, 'k.')
    axes[1].set_xlim((-25, 25))
    axes[1].set_ylim((-25, 25))
    axes[1].set_xlabel('East')
    axes[1].set_ylabel('North')
    axes[1].set_title(r'Error ($3\sigma$) in SP path wrt DGPS.')
    axes[1].set_aspect('equal')

    fig_fname = 'path-' + fig_fname_end

    if not os.path.exists(FIG_DIR):
        os.makedirs(FIG_DIR)

    fig.savefig(os.path.join(FIG_DIR, fig_fname))
    plt.close(fig)


def plot_dist_speed():

    def ce(error):
        return np.abs(np.mean(error))

    def rmse(error):
        return np.sqrt(np.mean(error**2))

    fig, axes = plt.subplots(4, 1, sharex=True, figsize=(12, 8))

    dgps_time = df_dgps_clean.unix_time - df_dgps_clean.unix_time[0]
    sp_time = df_sp_clean.log_time - df_sp_clean.log_time[0]

    # distance
    axes[0].plot(dgps_time, df_dgps_clean.pos, label='DGPS', color='black')
    axes[0].plot(sp_time, df_sp_clean.y_boat, label='SP')
    axes[0].set_xlabel('Time [s]')
    axes[0].set_ylabel('Distance [m]')
    title = 'Distance and Speed Comparisons\n{}\n{}'
    axes[0].set_title(title.format(m['rower'] + ' ' + str(m['stroke_rate']) +
                                   m['direction'], fname))
    axes[0].legend()

    # distance error
    dist_interp = interp1d(df_dgps_clean.unix_time, df_dgps_clean.pos)
    df_sp_clean_red = df_sp_clean.drop_duplicates(subset='location_latitude')
    dist_error = df_sp_clean_red.y_boat - dist_interp(df_sp_clean_red.log_time)
    lab = 'SP Error, CE {:1.2f}, STD {:1.2f}, RMSE {:1.2f}'.format(
        ce(dist_error), np.std(dist_error), rmse(dist_error))
    axes[1].plot(df_sp_clean_red.log_time - df_sp_clean.log_time.iloc[0],
                 dist_error, '.', label=lab)
    axes[1].axhline(dist_error.mean(), color='black', label='Mean Error')
    axes[1].set_xlabel('Time [s]')
    axes[1].set_ylabel('Distance Error [m]')
    axes[1].legend()

    # speed
    dgps_lab = 'DGPS Derivative, ~{:1.0f} Hz'.format(mean_dgps_sample_rate)
    axes[2].plot(dgps_time, df_dgps_clean.speed, color='black', label=dgps_lab)
    # rolling mean over a single stroke (sec/stroke * 100 samples/sec)
    window_length = int(60.0 / m['stroke_rate'] * 100)
    roll_lab = 'DGPS Rolling Mean, ~{:1.0f} Hz'.format(mean_dgps_sample_rate)
    axes[2].plot(dgps_time,
                 df_dgps_clean.speed.rolling(window=window_length).mean(),
                 label=roll_lab)
    axes[2].plot(sp_time, sp_speed_hold,
                 label='SP Derivative, ~{:1.1f} Hz'.format(mean_sp_sample_rate))
    axes[2].plot(sp_time, df_sp_clean.location_speed,
                 label='SP Internal, ~{:1.1f} Hz'.format(mean_sp_sample_rate))
    axes[2].set_xlabel('Time [s]')
    axes[2].set_ylabel('Speed [m/s]')
    axes[2].legend()

    # speed error
    rmean_error = (df_dgps_clean.speed.rolling(window=window_length).mean() -
                   df_dgps_clean.speed)
    roll_err_lab = 'DGPS Rolling Mean Error, ~{:1.0f} Hz, RMSE={:1.2f}'.format(
        mean_dgps_sample_rate, rmse(rmean_error))
    axes[3].plot(sp_time, rmean_error, label=roll_err_lab)
    sp_deriv_error = sp_speed_hold - df_dgps_clean.speed
    axes[3].plot(sp_time, sp_deriv_error,
                 label='SP Derivative Error, ~{:1.1f} Hz, RMSE={:1.2f}'.format(
                    mean_sp_sample_rate, rmse(sp_deriv_error)))
    sp_speed_error = df_sp_clean.location_speed - df_dgps_clean.speed
    lab = ('SP Internal Error, ~{:1.1f} Hz, CE={:1.2f}, STD={:1.2f}, '
           'RMSE={:1.2f}').format(mean_sp_sample_rate, ce(sp_speed_error),
                                  np.std(sp_speed_error),
                                  rmse(sp_speed_error))
    axes[3].plot(sp_time, sp_speed_error, label=lab)
    axes[3].set_xlabel('Time [s]')
    axes[3].set_ylabel('Speed [m/s]')
    axes[3].legend()

    fig_fname = 'raw-dist-speed-' + fig_fname_end

    if not os.path.exists(FIG_DIR):
        os.makedirs(FIG_DIR)

    fig.savefig(os.path.join(FIG_DIR, fig_fname))
    plt.close(fig)


# Start the script

with open('metadata.yml') as f:
    metadata = yaml.load(f)

df_sp = {'club': pd.read_csv(os.path.join(DATA_DIR, 'amateur', 'iPhone',
                                          CLUB_SP_FNAME)),
         'elite': pd.read_csv(os.path.join(DATA_DIR, 'elite', 'iPhone',
                                           ELITE_SP_FNAME))}

cov_matrices = []

for fname in metadata.keys():
    print('Plotting: {}'.format(fname))

    m = metadata[fname]

    # TODO : Rename data directory from amateur to club.
    rower = 'amateur' if m['rower'] == 'club' else 'elite'
    df_dgps = pd.read_csv(os.path.join(DATA_DIR, rower, 'diffGPS', fname))

    if m['prepend_date_hour']:
        df_dgps.pc_time = (m['date'] + ' ' + m['pc_hour'] + ':' +
                           df_dgps.pc_time)
        df_dgps.gps_time = (m['date'] + ' ' + m['gps_hour'] + ':' +
                            df_dgps.gps_time)

    df_sp_clean, _, df_dgps_clean = clean_data(df_sp[m['rower']], df_dgps)

    mean_dgps_sample_rate = 1 / df_dgps_clean.unix_time.diff().mean()

    # Calculate the boat speed from the smartphone position derived distance
    # (labeled `y_boat`) in this data frame. Calculate the very crude backwards
    # difference based on when the smartphone GPS updates.
    sp_dist_updates = df_sp_clean.y_boat.drop_duplicates()
    sp_dist_times = df_sp_clean.log_time[sp_dist_updates.index]
    mean_sp_sample_rate = 1 / sp_dist_times.diff().mean()
    sp_speed = derivative(sp_dist_times, sp_dist_updates, method='backward',
                          padding='adjacent')
    interpolator = interp1d(sp_dist_times, sp_speed, kind='zero',
                            fill_value='extrapolate')
    sp_speed_hold = interpolator(df_sp_clean.log_time)

    fig_fname_end = ('-'.join([m['boat'], m['rower'], str(m['stroke_rate']),
                               m['direction']]) + '.png')
    plot_path()
    plot_heading()
    plot_dist_speed()
