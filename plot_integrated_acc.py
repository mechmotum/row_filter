import os

import yaml
import numpy as np
from scipy.integrate import cumtrapz
from scipy.optimize import curve_fit
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

from row_filter.clean_data import clean_data
from row_filter.complementary import realtime_filter

# DGPS : differential GPS
# SP : smartphone

DATA_DIR = 'notebooks/row_data'
ELITE_SP_FNAME = 'Boat-20180422T103229_1641_rpc364_data_1CLX_1_B_2CDF0487-83FC-45CC-B590-FF42D74E0D6D.csv'
CLUB_SP_FNAME = 'Boat2x-20180420T085713_1633_rpc364_data_1CLX_1_B_F92041BC-2503-4150-8196-2B45C0258ED8.csv'

with open('metadata.yml') as f:
    metadata = yaml.load(f)

error_data = []

df_sp = {'club': pd.read_csv(os.path.join(DATA_DIR, 'club-level', 'iPhone',
                                          CLUB_SP_FNAME)),
         'elite': pd.read_csv(os.path.join(DATA_DIR, 'elite', 'iPhone',
                                           ELITE_SP_FNAME))}

for fname in metadata.keys():
    print(fname)
    m = metadata[fname]
    if m['rower'] == 'club':
        rower = 'club-level'
    else:
        rower = 'elite'
    df_dgps = pd.read_csv(os.path.join(DATA_DIR, rower, 'diffGPS', fname))
    if m['prepend_date_hour']:
        df_dgps.pc_time = (m['date'] + ' ' + m['pc_hour'] + ':' +
                           df_dgps.pc_time)
        df_dgps.gps_time = (m['date'] + ' ' + m['gps_hour'] + ':' +
                            df_dgps.gps_time)
    df_sp_clean, df_acc_clean, df_dgps_clean = \
        clean_data(df_sp[m['rower']], df_dgps)
    time = df_acc_clean.log_time - df_acc_clean.log_time[0]
    v = cumtrapz(df_acc_clean.ay_boat, time, initial=0)
    v_low = realtime_filter(time, v, 0.0166, typ='low')
    v_high = realtime_filter(time, v, 0.0457, typ='high')
    d = cumtrapz(v, time, initial=0)
    distance_error = d - df_dgps_clean.pos
    error_data.append((time, distance_error))

    fig, ax = plt.subplots()
    fig.set_size_inches(5.25, 3.24)
    df = pd.DataFrame(data={'Integrated Acceleration': v,
                            'Low Frequency': v_low,
                            'High Frequency': v_high}, index=time)
    sns.set_style("whitegrid")
    sns.lineplot(data=df, palette=["#4878D0", "#EE854A", "#6ACC64"],
                 dashes=False)
    ax.set(xlabel='Time [s]', ylabel='Speed [$m/s$]')
    plt.tight_layout()
    fig_fname = ('integration-bias-' +
                 '-'.join([m['boat'], m['rower'], str(m['stroke_rate']),
                           m['direction']]) + '.png')
    fig.savefig('figures/{}'.format(fig_fname), dpi=300)

plt.figure()
for time, error in error_data:
    plt.plot(time, error)

# Fit a quadratic to the data.
times = np.hstack([t for t, e in error_data])
errors = np.hstack([e for t, e in error_data])


def f(x, a, b, c):
    return a*(x-b)**2 + c

(a, b, c), _ = curve_fit(f, times, errors)

plt.plot(error_data[0][0], f(error_data[0][0], a, b, c), linewidth=4,
         color='black')

plt.xlabel('Time [s]')
plt.ylabel('Error in distance estimate [m]')
plt.title('Difference in twice integrated boat acceleration and DGPS distance')

plt.show()
