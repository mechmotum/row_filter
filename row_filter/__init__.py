#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .version import __version__

#Basic Libraries
import numpy as np
import pandas as pd
from scipy.interpolate import *

#Custom Functions
from . import clean_data as cd
from . import clean_data_norm as cdn
from . import clean_data_stroke_accy as cds_accy
from . import complementary as cf
from . import comp_test as cft
from .detect_peaks import *
from . import kalman as kf
from . import phone_error_calc as pec
from . import signal_sync_error as sigsync
from . import optimize_filter as opt_filt


import seaborn as sns
from dtk.process import butterworth

#Plotting Functions
import matplotlib.pyplot as plt
import plotly as py
import plotly.graph_objs as go
