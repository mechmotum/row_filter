import pandas as pd
import numpy as np
from scipy.interpolate import interp1d
import pyproj

from . import signal_sync_error as sigsync


def calc_distance(x, y):
    """Returns the Euclidean distance traveled along a Cartesian path.

    Parameters
    ==========
    x : array_like, shape(n,)
        The Cartesian x coordinates as a function of monotonically increasing
        time.
    y : array_like, shape(n,)
        The Cartesian y coordinates as a function of monotonically increasing
        time, which correspond to the x coordinates.

    Returns
    =======
    d : ndarray, shape(n,)
        The distance along the travel path as a function of time.

    """
    x, y = np.asarray(x), np.asarray(y)
    d = np.zeros_like(x)
    ds = np.sqrt(np.diff(x)**2 + np.diff(y)**2)
    d[1:] = np.cumsum(ds)
    return d


def clean_phone(df_phone):
    """Returns a data frame containing the smartphone data with desired
    columns, no missing values, no duplicate measurements, and sorted by the
    phone's log time.

    Parameters
    ==========
    df_phone: DataFrame
        Smartphone raw data.

    Returns
    =======
    df_phone: DataFrame
        Cleaned smartphone data.

    """

    # Select columns to keep
    df_phone = df_phone.filter([
        'log_time',
        'location_speed',
        'location_latitude',
        'location_longitude',
        'location_altitude',
        'location_course',
        'location_heading_x',
        'location_heading_y',
        'location_heading_z',
        'location_true_heading',
        'location_magnetic_heading',
        'accelerometer_acceleration_x',
        'accelerometer_acceleration_y',
        'accelerometer_acceleration_z',
        'gyro_rotation_x',
        'gyro_rotation_y',
        'gyro_rotation_z',
        'motion_roll',
        'motion_pitch',
        'motion_yaw',
    ])

    # Drop rows if any NA values are present in the location_speed or
    # accelerometer_acceleration_y columns.
    df_phone = df_phone.dropna(subset=['location_speed',
                                       'accelerometer_acceleration_y'],
                               axis=0, how='any')

    # Drop rows that have duplicate time values
    df_phone = df_phone.drop_duplicates(subset='log_time')

    # Sort by log_time
    df_phone = df_phone.sort_values('log_time').reset_index(drop=True)

    return df_phone


def convert_lonlat_to_cartesian(longitude, latitude, location='epsg:3310'):
    """Returns the x and y Cartesian coordinates corresponding to the provided
    longitude and latitude at the given location.

    Parameters
    ==========
    longitude : array_like, shape(n,)
        The corresponding longitude values in degrees.
    latitude : array_like, shape(n,)
        The latitude values in degrees.
    location : string, optional
        A valid initialization string for PROJ4. The default is ``epsg:3310``
        which corresponds to the region in Calfornia where Washington Lake,
        West Sacramento falls.

    Returns
    =======
    x : ndarray, shape(n,)
        The east-west positions in meters.
    y : ndarray, shape(n,)
        The north-south positions in meters.

    """

    latitude = np.asarray(latitude)
    longitude = np.asarray(longitude)

    inproj = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    outproj = pyproj.Proj(init=location)

    return pyproj.transform(inproj, outproj, longitude, latitude,
                            radians=False)


def boat_yaxis_displacement(df_phone):
    '''
    Converts displacment in latlong to displacment in the boat's y-direction.

    Parameters
    ----------
    df_phone: DataFrame
        Cleaned dataframe from the phone

    Returns
    -------
    df_phone: DataFrame
        Dataframe with displacement in the direction of the boat in units of
        meters.

    '''
    x, y = convert_lonlat_to_cartesian(df_phone.location_longitude,
                                       df_phone.location_latitude)
    df_phone['east'] = x
    df_phone['north'] = y
    df_phone['y_boat'] = calc_distance(x, y)

    return df_phone


def create_phone_acc_df(df_phone):
    '''
    Create separate accelerometer dataframe. Selects relevant columns
    for accelerometer, drops nan values, and converts acceleration
    units from g's to m/s^2.
    Parameter
    ---------
    df_phone: Cleaned phone dataframe

    Return
    ------
    df_acc: phone Acceleration dataframe
    '''

    # Select columns to keep
    df_acc = df_phone.filter(['log_time', 'accelerometer_acceleration_y',
                              'accelerometer_acceleration_z'])

    # Drop nan values
    df_acc = df_acc.dropna(subset=['accelerometer_acceleration_y'], axis=0,
                           how='any')

    # Convert units from g's to m/s^2
    df_acc['ay_boat'] = df_acc['accelerometer_acceleration_y'] * -9.81
    df_acc['az_boat'] = df_acc['accelerometer_acceleration_z'] * -9.81

    return df_acc


def find_nearest_index(array, value):
    """Returns the index of the array entry closest to the provided value.

    Parameters
    ==========
    array : array_like, shape(n,)
        An array of monotonically increasing floats.
    value: float
        The value the nearest index is sought after.

    Return
    ------
    idx : integer
        The index of closest value in the array.

    """
    array = np.asarray(array)
    return np.argmin(np.abs(array - value))


def find_cruising_idxs(t, x, y, stroke_rate=22):
    """Returns the indices of xy that correspond to the times that bound the
    cruising speed.

    Parameters
    ==========
    t : array_like, shape(n,)
        Time in seconds.
    x : array_like, shape(n,)
        East-west position.
    y : array_like, shape(n,)
        North-south position.

    Returns
    =======
    start_idx : integer
    stop_idx : integer

    """

    def moving_average(a, n=3):
        ret = np.cumsum(a, dtype=float)
        ret[n:] = ret[n:] - ret[:-n]
        return ret[n - 1:] / n

    d = calc_distance(x, y)
    v = np.diff(d) / np.diff(t)
    v = np.hstack((v[0], v))

    v_avg = np.zeros_like(v)
    window_length = int(60.0 / stroke_rate * 10)
    v_avg[window_length-1:] = moving_average(v, n=window_length)
    v_min = v_avg[int(len(v_avg) / 2) - 25:int(len(v_avg)) + 25].mean() - 0.25

    if np.mean(v_avg[:10]) > v_min:
        cruising = True
        cruise_start_idx = 0
    else:
        cruising = False
    cruise_stop_idx = -1
    for i, val in enumerate(v_avg):
        if not cruising and val > v_min:
            cruise_start_idx = i
            cruising = True
        if cruising and val < v_min:
            cruise_stop_idx = i - 1
            break
    if cruise_stop_idx - cruise_start_idx < 100:
        cruise_start_idx = 0
        cruise_stop_idx = -1

    print(v_min)
    print(cruise_start_idx)
    print(cruise_stop_idx)
    print(len(x))

    return cruise_start_idx, cruise_stop_idx


def slice_phone(df_sp, df_dgps):
    """Returns truncated smartphone and DGPS data frames that span the DGPS
    "cruising" portion of the trial.

    Parameters
    ==========
    df_sp : DataFrame
        Cleaned smartphone data.
    df_dgps : DataFrame
        Differential GPS data with ``unix_time`` sycronized to the smartphone's
        ``log_time``.

    Returns
    =======
    df_sp_slice: DataFrame
        Truncated smartphone data.
    df_dgps_slice : DataFrame
        Truncated DGPS data.

    """

    # Find the indices for the trial start and stop times in the phone data
    # that correspond to the beginning and end of the DGPS data for that trial.
    trial_start_idx = find_nearest_index(df_sp.log_time,
                                         df_dgps.unix_time.iloc[0])
    trial_stop_idx = find_nearest_index(df_sp.log_time,
                                        df_dgps.unix_time.iloc[-1])

    # Slice the trial from the phone time series.
    df_sp_slice = df_sp.iloc[trial_start_idx:trial_stop_idx].reset_index(drop=True)

    # Using 2.7 m/s as the minimum speed seen during "cruising", find the
    # smartphone GPS updates that are nearest this cutoff.
    sp_updates = df_sp_slice[df_sp_slice.location_speed > 2.7].drop_duplicates(subset='location_latitude')
    # Take the second and second to last indices to ensure valid smartphone
    # updates.
    cruise_start_time = sp_updates.log_time.iloc[1]
    cruise_stop_time = sp_updates.log_time.iloc[-2]
    # Find the indices corresponding to the times.
    cruise_start_idx = find_nearest_index(df_sp_slice.log_time, cruise_start_time)
    cruise_stop_idx = find_nearest_index(df_sp_slice.log_time, cruise_stop_time)
    # Truncate the phone data to the cruising period.
    df_sp_slice = df_sp_slice.iloc[cruise_start_idx:cruise_stop_idx].reset_index(drop=True)

    # Drop the last 5 seconds to deal with drops in speed in some trials.
    df_sp_slice = df_sp_slice.iloc[:-500]

    # Truncate the DGPS data to match the smartphone slice.
    start_gps = find_nearest_index(df_dgps.unix_time,
                                   df_sp_slice.log_time.iloc[0])
    stop_gps = find_nearest_index(df_dgps.unix_time,
                                  df_sp_slice.log_time.iloc[-1])
    df_dgps_slice = df_dgps.iloc[start_gps:stop_gps].reset_index(drop=True)

    return df_sp_slice, df_dgps_slice


def slice_acc(df_acc, df_dgps):
    '''
    Cuts the phone array to match the start/stop time of the differential GPS.

    Parameter
    ---------
    df_acc: acc dataframe
    df_dgps: differential GPS dataframe

    Return
    ------
    df_phone_slice: acc dataframe sliced to slice of df_dgps
    '''
    start = find_nearest_index(df_acc.log_time, df_dgps.unix_time.iloc[0])
    stop = find_nearest_index(df_acc.log_time, df_dgps.unix_time.iloc[-1])
    df_acc_slice = df_acc.iloc[start:stop].reset_index()
    return df_acc_slice


def upsample_dgps(df_dgps, df_phone):
    """Returns a data frame containg the distance and speed derived from the
    DGPS resampled at the smartphone accelerometer sample times (approximately
    100 Hz).

    Parameter
    ---------
    df_dgps: DataFrame
        Differential GPS dataframe

    Return
    ------
    df_dgps_100Hz: DataFrame
        Differential GPS dataframe with position and speed columns.

    """
    # Calculate the distance and speed based on the ~10Hz raw data.
    distance_10hz = calc_distance(df_dgps["east(meters)"],
                                  df_dgps["north(meters)"])
    speed_10hz = np.diff(distance_10hz) / np.diff(df_dgps.unix_time.values)
    speed_10hz = np.hstack((speed_10hz[0], speed_10hz))

    # Create functions that interpolates the raw DGPS data (provided at ~10Hz).
    north_interp = interp1d(df_dgps.unix_time, df_dgps["north(meters)"],
                            fill_value='extrapolate')
    east_interp = interp1d(df_dgps.unix_time, df_dgps["east(meters)"],
                           fill_value='extrapolate')
    dist_interp = interp1d(df_dgps.unix_time, distance_10hz,
                           fill_value='extrapolate')
    speed_interp = interp1d(df_dgps.unix_time, speed_10hz,
                            fill_value='extrapolate')

    # Interpolate the DGPS data at the higher sample rate smartphone time
    # (~100Hz).
    df_dgps_100hz = pd.DataFrame()
    df_dgps_100hz['unix_time'] = df_phone.log_time
    # TODO : Remove the `(meters)` portion of the string.
    df_dgps_100hz['north(meters)'] = north_interp(df_phone.log_time)
    df_dgps_100hz['east(meters)'] = east_interp(df_phone.log_time)
    # TODO : Remove `pos` (we should use consistent wording with the paper)
    df_dgps_100hz['pos'] = dist_interp(df_phone.log_time)
    df_dgps_100hz['distance'] = dist_interp(df_phone.log_time)
    df_dgps_100hz['speed'] = speed_interp(df_phone.log_time)

    return df_dgps_100hz


# Master Cleaning Function: clean_data calls all functions above
def clean_data(df_phone, df_dgps):
    """Returns three syncronized data frames corresponding to a single trial.

    1. Cleans the phone data.
    2. Adds a Unix Epoch timestamp to the DGPS data based on GPS time.
    3. Slices the phone data to the same time period as the DGPS file.
    4. Interpolates the DGPS data at the Smartphone accelerometer update times.
    5. Calculates the Cartesian coordinates and distance traveled from the
       Smartphone GPS.
    6. Extracts the accelerometer columns from the Smartphone data.

    Parameters
    ==========
    df_phone: DataFrame
        A data frame generated from the raw Swiftrow smartphone application.
        This includes all trials for a day.
    df_dgps: DataFrame
        A data frame generated from the raw differential GPS system for a
        single trial.

    Returns
    =======
    df_phone: DataFrame
        Smartphone data cleaned and sampled at the Smartphone accelerometer
        updates sample rate. The ``log_time`` column specifies the Unix Epoch
        UTC time in seconds. The Smartphone GPS data is interpolated as
        piecewise constant wrt to the accelerometer update times.
    df_acc: DataFrame
        Smartphone accelerometer data cleaned.
    df_dgps: DataFrame
        Differential GPS data cleaned and upsampled to the accelerometer sample
        rate via interpolation.

    """

    df_phone = clean_phone(df_phone)

    df_dgps = sigsync.to_unix(df_dgps)
    # TODO : This 18 second correction belongs in the to_unix function.
    df_dgps.unix_time = df_dgps.unix_time - 18
    df_phone, df_dgps = slice_phone(df_phone, df_dgps)
    df_dgps = upsample_dgps(df_dgps, df_phone)

    df_phone = boat_yaxis_displacement(df_phone)
    df_acc = create_phone_acc_df(df_phone)

    return df_phone, df_acc, df_dgps
