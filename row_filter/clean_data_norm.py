from dtk.process import butterworth
from . import clean_data as cd
from . import complementary as cf
from . import kalman as kf
from . import clean_data_stroke_accy as cds
from scipy.interpolate import interp1d
import numpy as np
import pandas as pd

def norm(df_acc, df_stroke):
    #Low pass the ay_boat signal
    cutoff_freq_low = 2;  # Hz
    sample_rate_low = 100; #Hz
    df_acc["ay_boat_filt"] = butterworth(df_acc.ay_boat, cutoff_freq_low, sample_rate_low, btype='lowpass')
    #Create an empty dataframe to store data
    df_norm = pd.DataFrame()
        #For each stroke
    for stroke_num in range(len(df_stroke.stroke_label)-1):
            # Get the indices of the start of stroke and end of stroke
        df_sliced = df_acc[df_stroke.sliced_idx[stroke_num]:df_stroke.sliced_idx[stroke_num+1]]
            # Get the min and max times of the stroke
        x_min = df_sliced['log_time'].min()
        x_max = df_sliced['log_time'].max()
            #Get the length of the stroke in seconds
        x_range = x_max - x_min
            #Normalize the stroke
        norm_stroke = (df_sliced.log_time.values - x_min)/(x_range)
            #Place the data in it's own dataframe
        df_norm1 = pd.DataFrame()
        df_norm1["ay_boat"] = df_sliced.ay_boat_filt
        df_norm1["norm"] = norm_stroke
        df_norm1["stroke_label"] = df_stroke.stroke_label[stroke_num]
        df_norm1["diffGPS_stamp"] = df_stroke.diffGPS_stamp[stroke_num]
        frames = [df_norm, df_norm1]
            #Append it to the other strokes
        df_norm = pd.concat(frames, ignore_index=True)
        #Get unique stroke numbers, remove stroke 0 because it is sometimes not a full stroke
    df_norm = df_norm[df_norm.stroke_label != 0]
    strokes = df_norm.stroke_label.unique()
        #Take the first stroke and place in it's own dataframe
    df_stroke0 = df_norm[df_norm.stroke_label == 1].reset_index(drop = True)
        #Build an empty dataframe for stroke 0 to be 'ground truth' for interpolation
    df_norm_interp = pd.DataFrame()
    df_norm_interp['ay_boat'] = df_stroke0.ay_boat
    df_norm_interp['norm'] = df_stroke0.norm
    df_norm_interp['stroke_label'] = df_stroke0.stroke_label
        #For each stroke, interpolate to the same normalization value
    for stroke_num in range(len(strokes)-1):
        df_nextstroke = df_norm[df_norm.stroke_label == (stroke_num +1)]
        poly_interp_acc = interp1d(df_nextstroke.norm, df_nextstroke.ay_boat)
        ay_boat = poly_interp_acc(df_stroke0.norm)
        stroke_interp = pd.DataFrame()
        stroke_interp['ay_boat'] = ay_boat
        stroke_interp['norm'] = df_stroke0.norm
        stroke_interp['stroke_label'] = (stroke_num + 1)
            #Append each stroke dataframe to the previous data
        df_norm_interp = pd.concat([df_norm_interp, stroke_interp], ignore_index=True)
        #Find the mean acc_y values of the normalization data
    norm_mean = df_norm_interp.groupby('norm').mean().reset_index()
    norm_mean.stroke_label = 100
    df_norm = pd.concat([df_norm_interp, norm_mean], axis =0, sort=True).reset_index(drop=True)
    #multiply by 100 to get on percentage scale
    df_norm.norm = df_norm.norm *100
    return df_norm

def norm_speed(df_dgps, df_stroke):
    #Create an empty dataframe to store data
    df_norm = pd.DataFrame()
    #For each stroke
    for stroke_num in range(len(df_stroke.stroke_label)-1):
        # Get the indices of the start of stroke and end of stroke
        df_sliced = df_dgps[df_stroke.sliced_idx[stroke_num]:df_stroke.sliced_idx[stroke_num+1]]
        # Get the min and max times of the stroke
        x_min = df_sliced['unix_time'].min()
        x_max = df_sliced['unix_time'].max()
        #Get the length of the stroke in seconds
        x_range = x_max - x_min
        #Normalize the stroke
        norm_stroke = (df_sliced.unix_time.values - x_min)/(x_range)
        #Place the data in it's own dataframe
        df_norm1 = pd.DataFrame()
        df_norm1["dgps_speed"] = df_sliced.speed
        df_norm1["norm"] = norm_stroke
        df_norm1["stroke_label"] = df_stroke.stroke_label[stroke_num]
        df_norm1["diffGPS_stamp"] = df_stroke.diffGPS_stamp[stroke_num]
        frames = [df_norm, df_norm1]
        #Append it to the other strokes
        df_norm = pd.concat(frames, ignore_index=True)
    #Get unique stroke numbers, remove stroke 0 because it is sometimes not a full stroke
    df_norm = df_norm[df_norm.stroke_label != 0]
    strokes = df_norm.stroke_label.unique()
    #Take the first stroke and place in it's own dataframe
    df_stroke0 = df_norm[df_norm.stroke_label == 1].reset_index(drop = True)
    #Build an empty dataframe for stroke 0 to be 'ground truth' for interpolation
    df_norm_interp = pd.DataFrame()
    df_norm_interp['dgps_speed'] = df_stroke0.dgps_speed
    df_norm_interp['norm'] = df_stroke0.norm
    df_norm_interp['stroke_label'] = df_stroke0.stroke_label
    #For each stroke, interpolate to the same normalization value
    for stroke_num in range(len(strokes)-1):
        df_nextstroke = df_norm[df_norm.stroke_label == (stroke_num +1)]
        poly_interp_acc = interp1d(df_nextstroke.norm, df_nextstroke.dgps_speed)
        dgps_speed = poly_interp_acc(df_stroke0.norm)
        stroke_interp = pd.DataFrame()
        stroke_interp['dgps_speed'] = dgps_speed
        stroke_interp['norm'] = df_stroke0.norm
        stroke_interp['stroke_label'] = (stroke_num + 1)
        #Append each stroke dataframe to the previous data
        df_norm_interp = pd.concat([df_norm_interp, stroke_interp], ignore_index=True)
    #Find the mean acc_y values of the normalization data
    norm_mean = df_norm_interp.groupby('norm').mean().reset_index()
    norm_mean.stroke_label = 100
    df_norm = pd.concat([df_norm_interp, norm_mean], axis =0, sort=True).reset_index(drop=True)
    #multiply by 100 to get on percentage scale
    df_norm.norm = df_norm.norm *100
    return df_norm


def clean_data(df_dgps, df_stroke, df_acc, df_dgps_stamp):
    """Returns a data frame containing the boat mounted smartphone Y
    acceleration component.

    Parameters
    ==========
    df_phone : DataFrame
        Raw data collected from the Swiftrow application of all trials.
    df_dgps : DataFrame
        Raw data collected from the Piksi DGPS for a single trial. The start
        and stop GPS satellite times of this must be in between the start and
        stop times in the ``df_phone`` data frame. The ``pc_time`` and
        ``gps_time`` columns must have the correct dates and hours of the day
        prepended to each time string.
    df_dgps_stamp : string
        Date time specification specification in GPS satellite time that the
        single trial in ``df_dgps`` started.

    Returns
    =======
    df_norm : DataFrame
        A data frame that provides the boat mounted smartphone Y acceleration
        component at each recorded time. The index is a monotonically
        increasing integer for each accelerometer reading and the columns are:
            ay_boat : float
                Boat mounted smartphone Y component of acceleration.
            norm : float
                Percent of stroke duration corresponding to each acceleration
                reading.
            stroke_label : integer
                Integer monotonically increasing label of the strokes in the
                trial.
            diffGPS_stamp : string
                Date time specification specification in GPS satellite time
                that the single trial in ``df_dgps`` started. Same for all
                rows.

    """
    df_norm_acc = norm(df_acc, df_stroke)
    df_norm_acc['diffGPS_stamp'] = df_dgps_stamp
    
    df_norm_speed = norm_speed(df_dgps, df_stroke)
    df_norm_speed['diffGPS_stamp'] = df_dgps_stamp
    
    return df_norm_acc, df_norm_speed


def clean_data_speed(df_phone, df_dgps, df_acc, df_dgps_stamp):
    """Returns a data frame containing the boat speed as a function of percent
    stroke.

    Parameters
    ==========
    df_phone : DataFrame
        Raw data collected from the Swiftrow application of all trials.
    df_dgps : DataFrame
        Raw data collected from the Piksi DGPS for a single trial. The start
        and stop GPS satellite times of this must be in between the start and
        stop times in the ``df_phone`` data frame. The ``pc_time`` and
        ``gps_time`` columns must have the correct dates and hours of the day
        prepended to each time string.
    df_dgps_stamp : string
        Date time specification specification in GPS satellite time that the
        single trial in ``df_dgps`` started.

    Returns
    =======
    df_norm : DataFrame
        A data frame that provides the boat speed at each recorded time. The
        index is a monotonically increasing integer for each accelerometer
        reading and the columns are:
            dgps_speed : float
                Boat speed computed from the DGPS position.
            norm : float
                Percent of stroke duration corresponding to each speed reading.
            stroke_label : integer
                Integer monotonically increasing label of the strokes in the
                trial.
            diffGPS_stamp : string
                Date time specification specification in GPS satellite time
                that the single trial in ``df_dgps`` started. Same for all
                rows.

    """
    # TODO : This runs all of the same calculations as clean_data(). Ideally it
    # would only run each necessary computation once.
    #df_phone, df_acc, df_dgps = cd.clean_data(df_phone, df_dgps)
    df_kalman = kf.compute(df_phone, df_acc)
    df_comp = cf.ton_ext(df_phone)
    df_stroke, stroke_idx = cds.stroke_data(df_phone, df_kalman, df_comp,
                                            df_dgps, 'df_dgps_stamp')
    df_norm = norm_speed(df_dgps, df_phone, df_stroke)
    df_norm['diffGPS_stamp'] = df_dgps_stamp
    return df_norm

#Use this function once all trials info has been appended to one dataframe at the end of Normalize_Stroke dataframe
def create_norm_df(df_norm_concat):
    single = df_norm_concat['diffGPS_stamp'].str[:8] == '20180422'
    df_norm_concat['rower'] = 'Olympian'
    df_norm_concat['rower'] = df_norm_concat.rower[single]
    df_norm_concat.rower = df_norm_concat.rower.fillna('Amateur')
    df_norm_concat.norm = df_norm_concat.norm
    df_dgpsinfo = pd.DataFrame()
    df_dgpsinfo['stamp'] = df_norm_concat.diffGPS_stamp.unique()
    df_dgpsinfo['SR_desired'] = [16,16,20,20,24,24,28,34,20,22,22,24,24,26,26]
    df_dgpsinfo['direction'] = ['NW','SE','NW','SE','NW','SE','NW','SE','NW','NW','SE','NW','SE','NW','SE']
    df_norm_concat['SR_desired'] = np.zeros(len(df_norm_concat))
    df_norm_concat['direction'] = np.zeros(len(df_norm_concat))
    for i in range(0, len(df_dgpsinfo.direction)):
        logic = df_dgpsinfo.stamp[i] == df_norm_concat.diffGPS_stamp
        column_name = 'SR_desired'
        column_direc = 'direction'
        df_slice = df_norm_concat[logic]
        df_norm_concat.loc[logic, column_name] = df_dgpsinfo.SR_desired[i]
        df_norm_concat.loc[logic, column_direc] = df_dgpsinfo.direction[i]
    df_norm_concat['rate_direction'] = df_norm_concat.SR_desired.apply(str).str.cat(df_norm_concat.direction)
    return df_norm_concat
