import numpy as np
from . import clean_data as cd
from dtk.process import butterworth
from . import detect_peaks as dp
from . import clean_data as cd
from . import kalman as kf
from . import complementary as cf
import pandas as pd


def findRMSE(array1, array2):
    RMSE = np.sqrt(np.mean(np.square(array1 - array2)))
    return RMSE


def stroke_data(df_phone, df_kalman, df_comp, df_dgps, dgps_stamp):
    '''
    Determines the start of stroke indices and creates a new df with kalman distance and indexes

    Parameter:
    ----------
    df_phone: cleaned mounted df_phone dataframe
    df_waist: cleaned waist phone dataframe
    df_kalman: kalman filter dataframe
    df_dgps: dgps dataframe after slice

    Return
    ------
    df_stroke: dataframe with phone index of start of stroke and kalman distance columns.
    '''
    # acceleration in the y axis
    acc_y = np.array(df_phone.accelerometer_acceleration_y)
    # low-pass the acc_y to exclude noise
    cutoff_freq_low = 2  # Hz
    sample_rate_low = 100 #Hz
    acc_y_low = butterworth(acc_y, cutoff_freq_low, sample_rate_low, btype='lowpass')
    x2 = -acc_y_low
    # set mph(minimum peak height) to .1 and mpd(minimum peak distance) to 150
    stroke_idx = dp.detect_peaks(x2, mph=.2, mpd=150, show=False)

    # stroke distance
    comp_stroke_distance = np.diff(df_comp.position[stroke_idx])
    kalman_stroke_distance = np.diff(df_kalman.position[stroke_idx])
    phone_stroke_distance = np.diff(df_phone.y_boat[stroke_idx])
    dgps_stroke_distance = np.diff(df_dgps.pos[stroke_idx])
    stroke_time = np.diff(df_kalman.log_time[stroke_idx])

    #find average filter velocity
    phone_avg_vel = []
    KF_avg_vel = []
    CF_avg_vel = []
    dgps_avg_vel = []
    for i in range(len(stroke_idx) - 1):
        KF_avg_vel.append(np.mean(df_kalman.velocity[stroke_idx[i] : stroke_idx[i+1]]))
        CF_avg_vel.append(np.mean(df_comp.velocity[stroke_idx[i] : stroke_idx[i+1]]))
        phone_avg_vel.append(np.mean(df_phone.location_speed[stroke_idx[i] : stroke_idx[i+1]]))
        dgps_avg_vel.append(np.mean(df_dgps.speed[stroke_idx[i] : stroke_idx[i+1]]))

    #Mean KF bias for the stroke
    bias = []
    for i in range(len(stroke_idx) - 1):
        bias.append(np.mean(df_kalman.bias[stroke_idx[i] : stroke_idx[i+1]]))

    #Build dataframe of all values
    df_stroke = pd.DataFrame()
    #df_stroke['stroke_idx'] = df_phone["index"].values[stroke_idx[1:]]
    df_stroke['stroke_idx'] = stroke_idx[1:] # Issue 46
    df_stroke['sliced_idx'] = stroke_idx[1:]
    df_stroke['stroke_label'] = df_stroke.index.values + 1
    df_stroke['stroke_duration'] = stroke_time
    df_stroke['SR_calc']= 60/df_stroke.stroke_duration
    df_stroke['comp_dist'] = comp_stroke_distance
    df_stroke['kalman_dist'] = kalman_stroke_distance
    df_stroke['phone_dist'] = phone_stroke_distance
    df_stroke['dgps_dist'] = dgps_stroke_distance
    df_stroke['KF_avg_vel'] = KF_avg_vel
    df_stroke['CF_avg_vel'] = CF_avg_vel
    df_stroke['phone_avg_vel'] = phone_avg_vel
    df_stroke['dgps_avg_vel'] = dgps_avg_vel
    df_stroke['bias'] = bias
    df_stroke['diffGPS_stamp'] = dgps_stamp

    return df_stroke, stroke_idx

def stroke_RMSE(df_phone, df_kalman, df_comp, df_dgps, stroke_idx, df_stroke):
    phone_vel_RMSE = []
    phone_pos_RMSE = []
    CF_vel_RMSE = []
    CF_pos_RMSE = []
    KF_vel_RMSE = []
    KF_pos_RMSE = []
    for i in range(len(stroke_idx) - 1):
        #Get the phone and dgps slice
        df_phone_slice = df_phone[stroke_idx[i] : stroke_idx[i+1]].reset_index(drop=True)
        df_dgps_slice = df_dgps[stroke_idx[i] : stroke_idx[i+1]].reset_index(drop=True)
        df_kalman_slice = df_kalman[stroke_idx[i] : stroke_idx[i+1]].reset_index(drop=True)
        df_comp_slice = df_comp[stroke_idx[i] : stroke_idx[i+1]].reset_index(drop=True)
        #find the vel rmse of the phone/kf/cf & append to array
        phone_vel_RMSE.append(findRMSE(df_phone_slice.location_speed, df_dgps_slice.speed))
        CF_vel_RMSE.append(findRMSE(df_comp_slice.velocity, df_dgps_slice.speed))
        KF_vel_RMSE.append(findRMSE(df_kalman_slice.velocity, df_dgps_slice.speed))
        #Find position rmse of the phone & append to array
        phone_pos_RMSE.append(findRMSE(df_phone_slice.y_boat, df_dgps_slice.pos))
        CF_pos_RMSE.append(findRMSE(df_comp_slice.position, df_dgps_slice.pos))
        KF_pos_RMSE.append(findRMSE(df_kalman_slice.position, df_dgps_slice.pos))
    df_stroke['CF_vel_RMSE'] = CF_vel_RMSE
    df_stroke['KF_vel_RMSE'] = KF_vel_RMSE
    df_stroke['phone_vel_RMSE'] = phone_vel_RMSE
    df_stroke['CF_pos_RMSE'] = CF_pos_RMSE
    df_stroke['KF_pos_RMSE'] = KF_pos_RMSE
    df_stroke['phone_pos_RMSE'] = phone_pos_RMSE

    return df_stroke

def clean_data(df_phone, df_dgps, df_acc, dgps_stamp):
    #df_phone, df_acc, df_dgps = cd.clean_data(df_phone, df_dgps)
    df_kalman = kf.compute(df_phone,df_acc)
    df_comp = cf.ton_ext(df_phone)
    df_stroke, stroke_idx = stroke_data(df_phone, df_kalman, df_comp, df_dgps, dgps_stamp)
    df_stroke = stroke_RMSE(df_phone, df_kalman, df_comp, df_dgps, stroke_idx, df_stroke)
    return df_stroke

def create_stroke_df(df_stroke):
    #position relative to dGPS
    df_stroke['CF_rel_pos'] = np.abs(df_stroke.comp_dist - df_stroke.dgps_dist)
    df_stroke['KF_rel_pos'] = np.abs(df_stroke.kalman_dist - df_stroke.dgps_dist)
    df_stroke['phone_rel_pos'] = np.abs(df_stroke.phone_dist - df_stroke.dgps_dist)
    #classify as a single or double day
    single = df_stroke['diffGPS_stamp'].str[:8] == '20180422'
    df_stroke['rower'] = 'Elite'
    df_stroke['rower'] = df_stroke.rower[single]
    df_stroke.rower = df_stroke.rower.fillna('Club-level')
    #create diffgps information dataframe
    df_dgpsinfo = pd.DataFrame()
    df_dgpsinfo['stamp'] = df_stroke.diffGPS_stamp.unique()
    single = df_dgpsinfo['stamp'].str[:8] == '20180422'
    df_dgpsinfo['rower'] = 'Elite'
    df_dgpsinfo['rower'] = df_dgpsinfo.rower[single]
    df_dgpsinfo.rower = df_dgpsinfo.rower.fillna('Club-level')
    #desired stroke rate and the direction
    df_dgpsinfo['SR_desired'] = [16,16,20,20,24,24,28,28,34,20,22,22,24,24,26,26]
    df_dgpsinfo['direction'] = ['NW','SE','NW','SE','NW','SE','NW','SE','SE','NW','NW','SE','NW','SE','NW','SE']
    df_dgpsinfo['kf_constant'] = [5,0,5,9,6,5,0,9,14,0,3,0,0,10,0,5]
    df_dgpsinfo['rate_direction'] = df_dgpsinfo.SR_desired.apply(str).str.cat(df_dgpsinfo.direction)
    #Creates the logic for placing the info in the right section of the dataframe
    df_stroke['SR_desired'] = np.zeros(len(df_stroke))
    df_stroke['direction'] = np.zeros(len(df_stroke))
    df_stroke['kf_constant'] = np.zeros(len(df_stroke))
    for i in range(0, len(df_dgpsinfo.direction)):
        logic = df_dgpsinfo.stamp[i] == df_stroke.diffGPS_stamp
        column_name = 'SR_desired'
        column_direc = 'direction'
        column_kf = 'kf_constant'
        df_slice = df_stroke[logic]
        kf_logic = df_slice.stroke_label >= df_dgpsinfo.kf_constant[i]
        df_stroke.loc[logic, column_name] = df_dgpsinfo.SR_desired[i]
        df_stroke.loc[logic, column_direc] = df_dgpsinfo.direction[i]
        df_stroke.loc[logic, column_kf] = kf_logic
    df_stroke = df_stroke[df_stroke.stroke_label != 0].reset_index(drop=True)
    return df_stroke, df_dgpsinfo
