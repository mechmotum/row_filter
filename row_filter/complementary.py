from scipy import zeros, signal
from scipy.interpolate import interp1d
from scipy import interpolate
import numpy as np
import pandas as pd
from scipy.integrate import cumtrapz


def realtime_filter(t, x, cutoff, typ='low'):
    """Returns a 2nd order low- or high-pass filtered signal. Executes a
    realtime capable filter as a batch process.

    Parameters
    ==========
    t : array_like, shape(n,)
        The monotonically increasing time values in seconds. The sample rate
        can be irregular.
    x : array_like, shape(n,)
        The time varying signal's values.
    cutoff : float
        Cutoff frequency in Hz.
    typ : string, optional
        Either ``low`` or ``high`` for the filter type. Defaults to low.

    Returns
    =======
    y : ndarray, shape(n,)
        The filtered time varying signal.

    """

    y = np.zeros_like(t)
    w = y.copy()

    if typ == 'low':
        rt_filter = tons_lowpass_filter
        args = x, y, w
    elif typ == 'high':
        rt_filter = tons_highpass_filter
        z = y.copy()
        args = x, w[1:], w, z[1:], z
    else:
        raise ValueError("'low' and 'high' are the only valid filter types.")

    for i, (ti, xi) in enumerate(zip(t[1:], x[1:])):
        dt = ti - t[i]
        res = rt_filter(cutoff, dt, xi, *(a[i] for a in args))
        if typ == 'low':
            y[i+1], w[i+1] = res
        else:
            y[i+1], w[i+1], z[i+1] = res

    return y


def tons_lowpass_filter(cutoff_freq, sample_time, x0, x1, y1, yd1):
    """Returns a 2nd order low pass Butterworth filtered value provided the
    prior unfiltered and filtered values. This is suitable for real-time
    filtering.

    Parameters
    ==========
    cutoff_freq : float
        Desired low pass cutoff frequency in Hertz.
    sample_time : float
        Time between current sample and prior sample in seconds.
    x0 : float
        Current unfiltered value.
    x1 : float
        Prior unfiltered value.
    y1 : float
        Prior filtered value.
    yd1 : float
        Prior derivative of the filtered value.

    Returns
    =======
    y0 : float
        Current filtered signal value.
    yd0 :
        Current filtered signal derivative.

    Notes
    =====

    These are the counts of the operations in the code:

    +     : 9
    -     : 4
    *     : 27
    /     : 8
    **    : 5
    sqrt  : 1
    ----------
    total : 54

    """

    # Compute coefficients of the state equation
    a = (2 * np.pi * cutoff_freq)**2
    b = np.sqrt(2) * 2 * np.pi * cutoff_freq

    # Integrate the filter state equation using the midpoint Euler method with
    # time step h
    h = sample_time
    denom = 4 + 2*h*b + h**2 * a

    A = (4 + 2*h*b - h**2*a)/denom
    B = 4*h/denom
    C = -4*h*a/denom
    D = (4 - 2*h*b - h**2*a)/denom
    E = 2*h**2*a/denom
    F = 4*h*a/denom

    y0 = A * y1 + B * yd1 + E*(x0 + x1) / 2
    yd0 = C * y1 + D * yd1 + F*(x0 + x1) / 2

    return y0, yd0


def tons_highpass_filter(cutoff_freq, sample_time, xi, xim1, z1i, z1im1, z2i,
                         z2im1):
    """Returns a 2nd order high pass Butterworth filtered value provided the
    prior unfiltered values. This is suitable for real-time filtering.

    Parameters
    ==========
    cutoff_freq : float
        Desired high pass cutoff frequency in Hertz.
    sample_time : float
        Time between current sample and prior sample in seconds.
    xi : float
        Current value of unfiltered signal.
    xim1 : float
        Prior value of unfiltered signal.
    z1im1: float
        Prior value of the first state.
    z2im1: float
        Prior value of the second state.

    Returns
    =======
    yi : float
        Current value of the filtered signal.
    z1i : float
        Current value of the first state.
    z21 : float
        Current value of the second state.

    Notes
    =====

    These are the counts of the operations in the code:

    +     : 10
    -     : 5
    *     : 23
    /     : 3
    **    : 2
    sqrt  : 1
    ----------
    total : 44

    """

    h = sample_time
    w0 = 2 * np.pi * cutoff_freq  # convert to radians

    a0 = np.sqrt(2)*h*w0
    a1 = h**2
    a2 = w0**2
    a3 = a1*a2
    a4 = 2*a0
    a5 = a3 + a4 + 4
    a6 = 1/a5
    a7 = a1*xi + a1*xim1 - a3*z2im1 + a4*z2im1 + 4*h*z1im1 + 4*z2im1
    a8 = a2*h

    z1i = a6*(a5*(-a0*z1im1 - a8*z2im1 + h*xi + h*xim1 + 2*z1im1) - a7*a8)/(a0 + 2)
    z2i = a6*a7
    yi = (z1i - z1im1) / h

    return yi, z1i, z2i


def ton_ext(df_phone):

    avg_pos_low = 0.1628
    avg_pos_high = 17.784
    avg_vel_low = 0.04137
    avg_vel_high = 0.04775

    cutoff_freq_pos = [avg_pos_low, avg_pos_high]
    cutoff_freq_vel = [avg_vel_low, avg_vel_high]

    df = df_phone.filter(['log_time','y_boat','location_speed','accelerometer_acceleration_y'])

    time = df.log_time
    pos = df.y_boat

    # low frequency velocity input = GPS SPEED
    sig_low_vel = df.location_speed[1:].reset_index(drop=True)
    # high frequency velocity input = ACCELEROMETER (once integrated), multiply by -9.81 to remove gravity term
    sig_high_vel = cumtrapz(df.accelerometer_acceleration_y, time) * (-9.81)

    # low frequency position input initialized as distance along path (extrapolated later)
    sig_low_pos = pos[2:].reset_index(drop=True)
    # high frequency position input set later after CF velocity calculated, initialized as accelerometer (twice integrated)
    sig_high_pos = cumtrapz(sig_high_vel, time[1:])


    # make sure all data same length
    time = time[2:].reset_index(drop=True)
    pos = pos[2:].reset_index(drop=True)
    sig_low_vel = sig_low_vel[1:]
    sig_high_vel = sig_high_vel[1:]


    ## POSITION ##
    # Lowpass
    tons_lowpass_filt_sig_pos = np.array(sig_low_pos)
    tons_lowpass_filt_sigd_pos = np.zeros_like(sig_low_pos)

    # Highpass
    tons_high_filt_sig_y_pos = np.array(sig_high_pos)
    tons_high_filt_sig_z1_pos = np.zeros_like(sig_high_pos)
    tons_high_filt_sig_z2_pos = np.zeros_like(sig_high_pos)


    ## VELOCITY ##
    # Lowpass
    tons_lowpass_filt_sig_vel = np.array(sig_low_vel)
    tons_lowpass_filt_sigd_vel = np.zeros_like(sig_low_vel)

    # Highpass
    tons_high_filt_sig_y_vel = np.array(sig_high_vel)
    tons_high_filt_sig_z1_vel = np.zeros_like(sig_high_vel)
    tons_high_filt_sig_z2_vel = np.zeros_like(sig_high_vel)

    t = []
    p = []
    extrapolated_pos = np.zeros_like(time)
    extrapolated_pos[0] = pos[0]
    extrapolated_pos[1] = pos[1]

    cf = [0,0]

    for i in range(2, len(time)):  # start at 3rd time sample
        if i==2 or pos[i-1] != pos[i]:
            t = np.append(t,time[i])
            p = np.append(p,pos[i])
        # start extrapolation when t array has 2 elements
        if len(t) >=2:
            ext_pos = interpolate.interp1d(t, p, fill_value='extrapolate')
            extrapolated_pos[i] = ext_pos(time[i])
        else:
            extrapolated_pos[i] = pos[i]
        # low frequency position input = EXTRAPOLATED DISTANCE along path
        sig_low_pos = extrapolated_pos

        ## VELOCITY ##
        tons_lowpass_filt_sig_vel[i], tons_lowpass_filt_sigd_vel[i] = \
                            tons_lowpass_filter(cutoff_freq_vel[0], time[i] - time[i - 1],
                                sig_low_vel[i], sig_low_vel[i-1],
                                tons_lowpass_filt_sig_vel[i-1], tons_lowpass_filt_sigd_vel[i-1])
        tons_high_filt_sig_y_vel[i], tons_high_filt_sig_z1_vel[i], tons_high_filt_sig_z2_vel[i] = \
                            tons_highpass_filter(cutoff_freq_vel[1], time[i] - time[i - 1],
                                sig_high_vel[i], sig_high_vel[i-1],
                                tons_high_filt_sig_z1_vel[i], tons_high_filt_sig_z1_vel[i-1],
                                tons_high_filt_sig_z2_vel[i], tons_high_filt_sig_z2_vel[i-1])

        # high frequency position input = CF VELOCITY (once integrated)
        cf.append(tons_lowpass_filt_sig_vel[i] + tons_high_filt_sig_y_vel[i])
        sig_high_pos[i] = sig_high_pos[i-1] + (cf[i] * (time[i] - time[i-1]))

        ## POSITION ##
        tons_lowpass_filt_sig_pos[i], tons_lowpass_filt_sigd_pos[i] = \
                            tons_lowpass_filter(cutoff_freq_pos[0], time[i] - time[i - 1],
                                sig_low_pos[i], sig_low_pos[i-1],
                                tons_lowpass_filt_sig_pos[i-1], tons_lowpass_filt_sigd_pos[i-1])
        tons_high_filt_sig_y_pos[i], tons_high_filt_sig_z1_pos[i], tons_high_filt_sig_z2_pos[i] = \
                            tons_highpass_filter(cutoff_freq_pos[1], time[i] - time[i - 1],
                                sig_high_pos[i], sig_high_pos[i-1],
                                tons_high_filt_sig_z1_pos[i], tons_high_filt_sig_z1_pos[i-1],
                                tons_high_filt_sig_z2_pos[i], tons_high_filt_sig_z2_pos[i-1])

    cf_pos = tons_lowpass_filt_sig_pos + tons_high_filt_sig_y_pos
    cf_vel = tons_lowpass_filt_sig_vel + tons_high_filt_sig_y_vel

    #Create new dataframe to return
    df_comp = pd.DataFrame()
    df_comp['log_time'] = time.reset_index(drop=True)
    df_comp['position'] = cf_pos
    df_comp['velocity'] = cf_vel
    df_comp['extrapolated_pos'] = sig_low_pos
    df_comp = df_comp.reset_index(drop=True)

    return df_comp
