import numpy as np
import pandas as pd

def compute(df_phone, df_acc):

    """
    Runs Kalman filter and computes estimates of position and velocity
    from iPhone position/velocity and acceleration.

    Coordinate system: y-axis relative to the boat.

    Parameters
    ----------
    df_phone: cleaned pandas dataframe from iPhone
    df_acc: cleaned pandas dataframe from iPhone accelerometer

    Returns
    -------
    df_kalman: pandas dataframe with kalman computed estimates of position, velocity, and accelerometer bias
    """

    #Initialization
    data_len = len(np.array(df_acc.log_time)) -1
    dt = np.diff(df_phone.log_time)              # dt array

    #States Matrix: x = [[Position],[Velocity],[Acceleration Bias]]
    x = np.zeros((data_len, 3, 1), dtype=np.float64)
    x[0] = [[df_phone.y_boat[0]],[df_phone.location_speed[0]],[.42]]   #Initialize

    #Accelerometer data: Used as input in model prediction step
    a = np.array(df_acc.ay_boat)

    #Phone position/velocity data: used to correct prediction in measurement update step
    df_update = df_phone[['y_boat', 'location_speed']].copy().values

    C = np.array([[1, 0, 0],[0, 1, 0]])
    #Q: process noise (undertainty of model)
    Q = np.array([[0, 0, 0], [0, 0, 0], [0, 0, 0]])

    #R: measurement noise (uncertainty of measurements)
    R = np.array([[25.279,0], [0,100]])

    #P: Covariance matrix of predicted values
    P = np.zeros((data_len, 3, 3))
    P[0] = np.diag([1, 1, 0.51])

    #L: Kalman Gain (Is a function of Q/(Q+R))
    L = np.zeros((data_len, 3, 2))

    #I: Identity matrix
    I = np.identity(3)

    #Filter

    for i in range(1, data_len):
        #Model prediction
        A = np.array([[1, dt[i],0],[0, 1, -dt[i]], [0,0,1]])
        B = np.array([[0], [dt[i]], [0]])
        x[i] = A @ x[i-1] + B * a[i-1]
        P[i] = A @ P[i-1] @ A.T + Q

        #Measurement update @ 100Hz
        if (i==0 or df_update[i-1,0] != df_update[i,0]):   #Uncomment if update at 0.3Hz
            L[i] = P[i] @ C.T @ np.linalg.inv(C @ P[i] @ C.T + R)
            x[i] = x[i] + L[i] @ (df_update[i].reshape(2,1) - C @ x[i])
#             P[i] = P[i] - L[i] @ C @ P[i]
            P[i] = (I - L[i] @ C) @ P[i] @ (I - L[i] @ C).T + L[i] @ R @ L[i].T # Joseph Equation


    #Create new dataframe to return
    df_kalman = pd.DataFrame()
    df_kalman['log_time'] = df_phone.log_time[:-1]
    df_kalman['position'] = np.concatenate(x[:, 0, :])
    df_kalman['velocity'] = np.concatenate(x[:, 1, :])
    df_kalman['bias'] = np.concatenate(x[:, 2, :])

    return df_kalman
