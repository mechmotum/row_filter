from scipy.optimize import least_squares
from scipy.interpolate import interp1d
import numpy as np

from . import comp_test as cf
from . import kalman as kf

def diff_pos(df_filter, df_dgps, df_phone):
    dgps_pos = df_dgps.pos - df_dgps.pos[0] + df_phone.y_boat[0] # start dgps and phone at same position
    poly_interp_pos = interp1d(df_dgps.unix_time, dgps_pos)
    dgps_pos_interp = poly_interp_pos(df_filter.log_time)
    diff = df_filter.position - dgps_pos_interp
    return diff

def diff_vel(df_filter, df_dgps):
    poly_interp_vel = interp1d(df_dgps.unix_time, df_dgps.speed)
    dgps_v_interp = poly_interp_vel(df_filter.log_time)
    diff = df_filter.velocity - dgps_v_interp
    return diff

# initial guess
cutoff_freq_pos = [0.5,3]
cutoff_freq_vel = [0.025,0.025]

def optimize_cutoff_freq_pos(df_phone, df_dgps, count):
    def error_pos(cutoff_freq_pos):
        df_ton = cf.ton(df_phone, cutoff_freq_pos, cutoff_freq_vel)
        e = 0
        e = diff_pos(df_ton, df_dgps,df_phone)
        return e
    sol_pos = least_squares(error_pos, cutoff_freq_pos)
    return sol_pos.x[0], sol_pos.x[1], sol_pos.cost, None


def optimize_cutoff_freq_vel(df_phone, df_dgps, count):
    def error_vel(cutoff_freq_vel):
        df_ton = cf.ton(df_phone, cutoff_freq_pos, cutoff_freq_vel)
        e = 0
        e = diff_vel(df_ton, df_dgps)
        return e
    sol_vel = least_squares(error_vel, cutoff_freq_vel)
    return sol_vel.x[0], sol_vel.x[1], sol_vel.cost, None



## EXTRAPOLATED ##

def optimize_cutoff_freq_pos_ext(df_phone, df_dgps, count):
    def error_pos(cutoff_freq_pos):
        df_ton = cf.ton_ext(df_phone, cutoff_freq_pos, cutoff_freq_vel)
        e = 0
        e = diff_pos(df_ton, df_dgps,df_phone)
        return e
    sol_pos = least_squares(error_pos, cutoff_freq_pos)
    return sol_pos.x[0], sol_pos.x[1], sol_pos.cost, None


def optimize_cutoff_freq_vel_ext(df_phone, df_dgps, count):
    def error_vel(cutoff_freq_vel):
        df_ton = cf.ton_ext(df_phone, cutoff_freq_pos, cutoff_freq_vel)
        e = 0
        e = diff_vel(df_ton, df_dgps)
        return e
    sol_vel = least_squares(error_vel, cutoff_freq_vel)
    return sol_vel.x[0], sol_vel.x[1], sol_vel.cost, None


## KALMAN ##

#initial guess
covariance = [14.823,0.624]

def optimize_kalman(df_phone_slice, df_acc, df_dgps, count):
    def error_kalman(covariance):
        df_kalman = kf.optimize_R(df_phone_slice, df_acc, covariance)
        W1 = 0
        W2 = 1
        e_vel = df_kalman.velocity - df_dgps.speed
        e_pos = df_kalman.position - df_dgps.pos
        e = W1*e_vel + W2*e_pos
        e = e[np.isfinite(e)]
        return e
    sol_vel = least_squares(error_kalman, covariance)
    return abs(sol_vel.x[0]), abs(sol_vel.x[1]), sol_vel.cost, None, None, None

