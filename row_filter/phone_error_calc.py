from math import *
import pandas as pd
import numpy as np
import pyproj
from . import clean_data as cd

def error_xy(df_phone, df_dgps, desired_SR,count, rower):
    df_phone, df_acc, df_dgps = cd.clean_data(df_phone, df_dgps)
    #Drop duplicate lat/long values in phone trial
    df_nodup = df_phone.drop_duplicates(subset = 'location_latitude', keep = 'first').reset_index()
    #Drop phone's duplicate values in gps trial
    df_dgps_nodup = df_dgps.iloc[df_nodup["index"].values].reset_index()
    #Find X-Y coordinates of the phone
    inproj = pyproj.Proj(proj='latlong', ellps='WGS84', datum='WGS84')
    outproj = pyproj.Proj(init='epsg:3310')    
    X, Y, Z = pyproj.transform(inproj, outproj, df_nodup.location_longitude.values, \
                               df_nodup.location_latitude.values, df_nodup.location_altitude.values, radians=False)
    #Initialize first data point to be at (0,0)
    north_phone = np.abs(Y - Y[0])
    east_phone = np.abs(X - X[0])
    north_dgps = np.abs(df_dgps_nodup["north(meters)"] - df_dgps_nodup["north(meters)"][0])
    east_dgps = np.abs(df_dgps_nodup["east(meters)"] - df_dgps_nodup["east(meters)"][0])
    #Find the distance travelled, d
    distance_phone = np.sqrt(north_phone**2 + east_phone**2)
    distance_dgps = np.sqrt(north_dgps**2 + east_dgps**2)
    #Calculate the error of each datastream
    df_error = pd.DataFrame()
    df_error['north_error'] = north_phone - north_dgps
    df_error['east_error'] = east_phone - east_dgps
    df_error['dist_error'] = distance_phone - distance_dgps
    df_error['speed_error'] = df_nodup.location_speed - df_dgps_nodup.speed
    df_error['desired_SR'] = desired_SR
    df_error['trial'] = count
    df_error['rower'] = rower
    #Remove the origin (0,0) position from the error df
    df_error = df_error.iloc[1:len(df_error)].reset_index(drop = True)
    
    return df_error