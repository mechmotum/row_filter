
# external dependencies
import numpy as np
from numpy.fft import fft, fftfreq
from scipy import __version__ as scipy_version
from scipy.integrate import trapz, cumtrapz
from scipy.interpolate import UnivariateSpline
from scipy.optimize import fmin
from scipy.signal import butter, filtfilt

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import time, calendar, datetime
import datetime as dt
from time import mktime


def to_unix(df):
    """Adds a column to the provided data frame called ``unix_time`` which is
    the corresponding seconds since epoch to the date time in the ``gps_time``
    column.

    Parameters
    ==========
    df : DataFrame
        Data frame with a column named ``gps_time``. ``gps_time`` is a date
        time string like ``2018-04-22 17:50:11.5``, which provides a GPS date
        and 24 hr time specification.

    Returns
    =======
    df : DataFrame
        Same data frame as passed in that now includes a ``unix_time`` column.

    Notes
    =====

    GPS time is the actual number of seconds since 1980-01-06 UTC and it is not
    perturbed by leap seconds, thus is ahead of UTC by 18 seconds. This 18
    second correction is not applied in this function.

    See http://leapsecond.com/java/gpsclock.htm for an explanation of GPS time.

    The SwiftNav Piksi reports GPS time to the tenth of the second in
    resolution.

    """

    # Convert GPS datetime string to datetime object
    gps_datetime = pd.to_datetime(df.gps_time)

    # Convert GPS datetime to "unix time", i.e. seconds since UNIX Epoch time.
    gps_time_unix = np.divide((gps_datetime - pd.Timestamp("1970-01-01"))
                              // pd.Timedelta(1, 'ms'), 1000)

    # Create new column with Unix time:
    df.insert(loc=1, column='unix_time', value=gps_time_unix)

    # Make sure data is sorted by Unix time:
    df = df.sort_values('unix_time').reset_index(drop=True)

    return df


def find_nearest_index(array,value):
    idx = (np.abs(array-value)).argmin()
    return idx

def find_start(df_gps, df_phone):
	# Function to output start/stop index for a boat run

	# returns index in phone data that correspnds to start of diff gps:
	start = find_nearest_index(df_phone.log_time.values, df_gps.unix_time.values[0])

	return start

def find_stop(df_gps, df_phone):
	end = df_gps.unix_time.values
	end = np.int(len(end) - 1)
	stop = find_nearest_index(df_phone.log_time.values, df_gps.unix_time.values[end])

	return stop


def speed(df):
	# Output speed of differential GPS and insert new column at end of df

    velocity_north = (df['north(meters)'].diff())/(df['tow(sec)'].diff())
    velocity_east = (df['east(meters)'].diff())/(df['tow(sec)'].diff())
    df['speed']=np.sqrt((velocity_north)**2+(velocity_east)**2)

    return df;

def acceleration(df):
	#Output acceleration of Diff GPS and insert new column at the end of df
    accel = (df['speed'].diff())/(df['tow(sec)'].diff());
    df['acceleration'] = accel

    return df;

def differential_shift(df):

	df.unix_time = df.unix_time[2:];
	df.acceleration = df.acceleration[2:];

	return




def signal_sync_error(signal1_time, signal1_Y, time, signal2_Y):

	'''
		Function to return the best estimate of error between 2 signals.
		Tau is assumed to be timeshift between signals form different sources

		Define:
		detla Y = error between 2 signals.

		INPUTS:




		tau_range: values of tau to test
		error_sum

	'''


	tau_range = np.linspace(0,50,50001);
	error_sum_array = np.empty(len(tau_range)); #pre-allocate
	df_timeError = pd.DataFrame(tau_range); #create DF, 1st col are Tau test values

	for i in range(len(tau_range)-1):

		shiftedTime = time + tau_range[i];
		signal1_newY = np.interp(shiftedTime, signal1_time, signal1_Y);

		error = abs(signal1_newY - signal2_Y);
		error_sum_array[i] = np.sum(error);

	# drop last value from error array: (why needed?)
	error_sum_array = error_sum_array[0:(len(error_sum_array))-1];

	# find minimum value of delta Y (error):
	min_error = np.nanmin(error_sum_array);

	#find index location of minimum error:
	min_index = np.nanargmin(error_sum_array);

	# add error sum values to dataframe:
	df_timeError['Error_Sum'] = pd.DataFrame(error_sum_array)

	# find tau value that corresponds to the minimum error:
	good_tau = df_timeError.iloc[min_index, 0];

	return good_tau
