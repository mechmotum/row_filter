import setuptools

exec(open('row_filter/version.py').read())

with open("README.rst", "r") as fh:
    long_description = fh.read()


install_requires = [
                    'numpy>=1.14.1',
                    'scipy>=1.0.0',
                    'pandas>=0.20.3',
                   ]

setuptools.setup(
    name="row_filter",
    version=__version__,
    author="Row Filter Authors",
    author_email="moorepants@gmail.com",
    description=("Python tool kit for estimating competitive rowing kinematic "
                 "metrics using smartphone-based sensing"),
    long_description=long_description,
    license='LICENSE.txt',
    url="https://gitlab.com/mechmotum/row_filter",
    packages=setuptools.find_packages(),
    extras_require={'doc': ['sphinx', 'numpydoc'],
                    'examples': [
                                 'matplotlib>=2.0',
                                 'seaborn>=0.9.0',
                                 'plotly',
                                 'notebook',
                                 'DynamicistToolKit>=0.5.3'
                                ]
                    },
    classifiers=['Development Status :: 3 - Alpha',
                 'Intended Audience :: Science/Research',
                 'Operating System :: OS Independent',
                 'Programming Language :: Python :: 3.6',
                 'Topic :: Scientific/Engineering :: Physics',
                 ],
)
